﻿using GPLPartnerConnect.Model;
using System;
using System.Collections.Generic;
using System.Data;
using GPLPartnerConnect.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL
{
    public class AdminDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        public DataSet AdminData(mAdmin adm, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_Admin", ref _sqlcommand,
                "@userid", SqlDbType.VarChar, adm.userid, ParameterDirection.Input,
                "@Name", SqlDbType.VarChar, adm.Name, ParameterDirection.Input,
                "@username", SqlDbType.VarChar, adm.username, ParameterDirection.Input,
                "@password", SqlDbType.VarChar, adm.password, ParameterDirection.Input,
                "@emailid", SqlDbType.VarChar, adm.emailid, ParameterDirection.Input,
                "@type", SqlDbType.VarChar, adm.type, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);
            return ds;
        }
    }
}
