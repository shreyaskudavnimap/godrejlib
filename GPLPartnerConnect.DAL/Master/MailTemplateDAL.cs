﻿using GPLPartnerConnect.Model.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Master
{
    public class MailTemplateDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess sqlDataAccess = new DataAccess();
        public DataSet Call_SP_MailTemplate(mMailTemplate objMTemplate, string flag)
        {
            DataSet ds = sqlDataAccess.GetDataSet("usp_MailTemplate", ref _sqlcommand ,
                    "@MailTemplateID", SqlDbType.VarChar, objMTemplate.MailTemplateID, ParameterDirection.Input,
                    "@MailTempleteCode", SqlDbType.VarChar, objMTemplate.MailTempleteCode, ParameterDirection.Input,
                    "@Subject", SqlDbType.VarChar, objMTemplate.Subject, ParameterDirection.Input,
                    "@Body", SqlDbType.VarChar, objMTemplate.Body, ParameterDirection.Input,
                    "@Active", SqlDbType.VarChar, objMTemplate.Active, ParameterDirection.Input,
                    "@InsertedBy", SqlDbType.VarChar, objMTemplate.InsertedBy, ParameterDirection.Input,
                    "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;
        }
    }
}