﻿using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class BrokerSFDCDataDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        public DataSet BrokerBookingData(mBrokerBooking obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_BrokerBooking", ref _sqlcommand,
                "@EmplID", SqlDbType.VarChar, obj.EmplID, ParameterDirection.Input,
                "@EmplContID", SqlDbType.VarChar, obj.EmplContID, ParameterDirection.Input,
                "@SFDC_id", SqlDbType.VarChar, obj.SFDC_id, ParameterDirection.Input,
                "@SFDC_PropStrengthPrimaryApplicantNameC", SqlDbType.VarChar, obj.SFDC_PropStrengthPrimaryApplicantNameC, ParameterDirection.Input,
                "@SFDC_PropStrengthProjectC", SqlDbType.VarChar, obj.SFDC_PropStrengthProjectC, ParameterDirection.Input,
                "@SFDC_ProjectNameC", SqlDbType.VarChar, obj.SFDC_ProjectNameC, ParameterDirection.Input,
                "@SFDC_PropStrengthBookingDateC", SqlDbType.VarChar, obj.SFDC_PropStrengthBookingDateC, ParameterDirection.Input,
                "@SFDC_PropStrengthRevisedAgreementAmountC", SqlDbType.VarChar, obj.SFDC_PropStrengthRevisedAgreementAmountC, ParameterDirection.Input,
                "@SFDC_RegistrationDateC", SqlDbType.VarChar, obj.SFDC_RegistrationDateC, ParameterDirection.Input,
                "@SFDC_PropStrengthBrokerAccountC", SqlDbType.VarChar, obj.SFDC_PropStrengthBrokerAccountC, ParameterDirection.Input,
                "@SFDC_PropStrengthBrokerContactC", SqlDbType.VarChar, obj.SFDC_PropStrengthBrokerContactC, ParameterDirection.Input,
                "@SFDC_BrokerPANNoC", SqlDbType.VarChar, obj.SFDC_BrokerPANNoC, ParameterDirection.Input,
                "@SFDC_TotalPaymentRealizedwrtTAVC", SqlDbType.VarChar, obj.SFDC_TotalPaymentRealizedwrtTAVC, ParameterDirection.Input,
                "@SFDC_name", SqlDbType.VarChar, obj.SFDC_name, ParameterDirection.Input,
                "@SFDC_lastmodifieddate", SqlDbType.VarChar, obj.SFDC_lastmodifieddate, ParameterDirection.Input,
                "@SFDC_PropstrengthPropertyNameC", SqlDbType.VarChar, obj.SFDC_PropstrengthPropertyNameC, ParameterDirection.Input,
                "@SFDC_SAPCustomerCreatedDateC", SqlDbType.VarChar, obj.SFDC_SAPCustomerCreatedDateC, ParameterDirection.Input,
                "@SFDC_Region", SqlDbType.VarChar, obj.SFDC_Region, ParameterDirection.Input,
                "@SFDC_house_unit", SqlDbType.VarChar, obj.SFDC_house_unit, ParameterDirection.Input,
                "@SFDC_tower_code", SqlDbType.VarChar, obj.SFDC_tower_code, ParameterDirection.Input,
                "@SFDC_Wing_block", SqlDbType.VarChar, obj.SFDC_Wing_Block, ParameterDirection.Input,
                "@SFDC_tower_name", SqlDbType.VarChar, obj.SFDC_tower_name, ParameterDirection.Input,
                "@SFDC_cip_tower", SqlDbType.VarChar, obj.SFDC_cip_tower, ParameterDirection.Input,
                "@application_booking_18", SqlDbType.VarChar, obj.application_booking_18, ParameterDirection.Input,
                "@booking_status", SqlDbType.VarChar, obj.booking_status, ParameterDirection.Input,
                "@PropStrength__Booking_Cancelled_Date__c", SqlDbType.VarChar, obj.PropStrength__Booking_Cancelled_Date__c, ParameterDirection.Input,
                "@B1P__c", SqlDbType.VarChar, obj.B1P__c, ParameterDirection.Input,
                "@B2P__c", SqlDbType.VarChar, obj.B2P__c, ParameterDirection.Input,
                "@B3P__c", SqlDbType.VarChar, obj.B3P__c, ParameterDirection.Input,
                "@B4P__c", SqlDbType.VarChar, obj.B4P__c, ParameterDirection.Input,
                "@B5P__c", SqlDbType.VarChar, obj.B5P__c, ParameterDirection.Input,
                "@B6P__c", SqlDbType.VarChar, obj.B6P__c, ParameterDirection.Input,
                "@B7P__c", SqlDbType.VarChar, obj.B7P__c, ParameterDirection.Input,
                "@TBP__c", SqlDbType.VarChar, obj.TBP__c, ParameterDirection.Input,
                "@PropStrength__Brokerage_Amount__c", SqlDbType.VarChar, obj.PropStrength__Brokerage_Amount__c, ParameterDirection.Input,
                "@title", SqlDbType.VarChar, obj.title, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input,
                "@propstrength__total_agreement_amount__c", SqlDbType.VarChar, obj.propstrength__total_agreement_amount__c, ParameterDirection.Input);
            
            return ds;
        }

        public DataSet BrokerBookingCancelData(mBrokerBooking obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_BrokerBookingCancel", ref _sqlcommand,
                "@EmplID", SqlDbType.VarChar, obj.EmplID, ParameterDirection.Input,
                "@EmplContID", SqlDbType.VarChar, obj.EmplContID, ParameterDirection.Input,
                "@SFDC_id", SqlDbType.VarChar, obj.SFDC_id, ParameterDirection.Input,
                "@SFDC_PropStrengthPrimaryApplicantNameC", SqlDbType.VarChar, obj.SFDC_PropStrengthPrimaryApplicantNameC, ParameterDirection.Input,
                "@SFDC_PropStrengthProjectC", SqlDbType.VarChar, obj.SFDC_PropStrengthProjectC, ParameterDirection.Input,
                "@SFDC_ProjectNameC", SqlDbType.VarChar, obj.SFDC_ProjectNameC, ParameterDirection.Input,
                "@SFDC_PropStrengthBookingDateC", SqlDbType.VarChar, obj.SFDC_PropStrengthBookingDateC, ParameterDirection.Input,
                "@SFDC_PropStrengthRevisedAgreementAmountC", SqlDbType.VarChar, obj.SFDC_PropStrengthRevisedAgreementAmountC, ParameterDirection.Input,
                "@SFDC_RegistrationDateC", SqlDbType.VarChar, obj.SFDC_RegistrationDateC, ParameterDirection.Input,
                "@SFDC_PropStrengthBrokerAccountC", SqlDbType.VarChar, obj.SFDC_PropStrengthBrokerAccountC, ParameterDirection.Input,
                "@SFDC_PropStrengthBrokerContactC", SqlDbType.VarChar, obj.SFDC_PropStrengthBrokerContactC, ParameterDirection.Input,
                "@SFDC_BrokerPANNoC", SqlDbType.VarChar, obj.SFDC_BrokerPANNoC, ParameterDirection.Input,
                "@SFDC_TotalPaymentRealizedwrtTAVC", SqlDbType.VarChar, obj.SFDC_TotalPaymentRealizedwrtTAVC, ParameterDirection.Input,
                "@SFDC_name", SqlDbType.VarChar, obj.SFDC_name, ParameterDirection.Input,
                "@SFDC_lastmodifieddate", SqlDbType.VarChar, obj.SFDC_lastmodifieddate, ParameterDirection.Input,
                "@SFDC_PropstrengthPropertyNameC", SqlDbType.VarChar, obj.SFDC_PropstrengthPropertyNameC, ParameterDirection.Input,
                "@SFDC_SAPCustomerCreatedDateC", SqlDbType.VarChar, obj.SFDC_SAPCustomerCreatedDateC, ParameterDirection.Input,
                "@SFDC_Region", SqlDbType.VarChar, obj.SFDC_Region, ParameterDirection.Input,
                "@SFDC_house_unit", SqlDbType.VarChar, obj.SFDC_house_unit, ParameterDirection.Input,
                "@SFDC_tower_code", SqlDbType.VarChar, obj.SFDC_tower_code, ParameterDirection.Input,
                "@SFDC_Wing_block", SqlDbType.VarChar, obj.SFDC_Wing_Block, ParameterDirection.Input,
                "@SFDC_tower_name", SqlDbType.VarChar, obj.SFDC_tower_name, ParameterDirection.Input,
                "@SFDC_cip_tower", SqlDbType.VarChar, obj.SFDC_cip_tower, ParameterDirection.Input,
                "@application_booking_18", SqlDbType.VarChar, obj.application_booking_18, ParameterDirection.Input,
                "@booking_status", SqlDbType.VarChar, obj.booking_status, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;
        }

        public DataSet BrokerEnquiryData(mBrokerEnquiry obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_BrokerEnquiry", ref _sqlcommand ,
                "@EmplID", SqlDbType.VarChar, obj.EmplID, ParameterDirection.Input,
                "@EmplContID", SqlDbType.VarChar, obj.EmplContID, ParameterDirection.Input,
                "@SFDC_id", SqlDbType.VarChar, obj.SFDC_id, ParameterDirection.Input,
                "@SFDC_Name", SqlDbType.VarChar, obj.SFDC_Name, ParameterDirection.Input,
                "@SFDC_ContactNameC", SqlDbType.VarChar, obj.SFDC_ContactNameC, ParameterDirection.Input,
                "@SFDC_PropStrengthProjectC", SqlDbType.VarChar, obj.SFDC_PropStrengthProjectC, ParameterDirection.Input,
                "@SFDC_MobileNumberC", SqlDbType.VarChar, obj.SFDC_MobileNumberC, ParameterDirection.Input,
                "@SFDC_PropStrengthRequestStatusC", SqlDbType.VarChar, obj.SFDC_PropStrengthRequestStatusC, ParameterDirection.Input,
                "@SFDC_OldCommentsC", SqlDbType.VarChar, obj.SFDC_OldCommentsC, ParameterDirection.Input,
                "@SFDC_MarketingProjectNameC", SqlDbType.VarChar, obj.SFDC_MarketingProjectNameC, ParameterDirection.Input,
                "@SFDC_PropStrengthBrokerAccountC", SqlDbType.VarChar, obj.SFDC_PropStrengthBrokerAccountC, ParameterDirection.Input,
                "@SFDC_PropStrengthBrokerContactC", SqlDbType.VarChar, obj.SFDC_PropStrengthBrokerContactC, ParameterDirection.Input,
                "@SFDC_DateOfSiteVisitC", SqlDbType.VarChar, obj.SFDC_DateOfSiteVisitC, ParameterDirection.Input,
                "@SFDC_lastmodifieddate", SqlDbType.VarChar, obj.SFDC_lastmodifieddate, ParameterDirection.Input,
                "@SFDC_DateOfEnquiry1C", SqlDbType.VarChar, obj.SFDC_DateOfEnquiry1C, ParameterDirection.Input,
                "@SFDC_BrokerPANNoC", SqlDbType.VarChar, obj.SFDC_BrokerPANNoC, ParameterDirection.Input,
                "@SFDC_PanNumber", SqlDbType.VarChar, obj.SFDC_PanNumber, ParameterDirection.Input,
                "@SFDC_Email", SqlDbType.VarChar, obj.SFDC_Email, ParameterDirection.Input,
                "@SFDC_Region", SqlDbType.VarChar, obj.SFDC_Region, ParameterDirection.Input,
                "@title", SqlDbType.VarChar, obj.title, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;
        }

        public DataSet BrokerBookingDataStatusData(mBrokerBookingDataStatus obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_BrokerBookingDataStatus", ref _sqlcommand ,
                "@EmplID", SqlDbType.VarChar, obj.EmplID, ParameterDirection.Input,
                "@EmplContID", SqlDbType.VarChar, obj.EmplContID, ParameterDirection.Input,
                "@DataCount", SqlDbType.VarChar, obj.DataCount, ParameterDirection.Input,
                "@LastSync", SqlDbType.VarChar, obj.LastSync, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;
        }

    }
}
