﻿using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class OTPDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess sqlDataAccess = new DataAccess();
        public DataSet Call_SP_OTP(mOTP objOTP, string flag)
        {
            DataSet ds = sqlDataAccess.GetDataSet("usp_OTP", ref _sqlcommand,
                    "@OTPID", SqlDbType.VarChar, objOTP.OTPID, ParameterDirection.Input,
                    "@pin", SqlDbType.VarChar, objOTP.pin, ParameterDirection.Input,
                    "@mobileNumber", SqlDbType.VarChar, objOTP.mobileNumber, ParameterDirection.Input,
                    "@EmplContID", SqlDbType.VarChar, objOTP.EmplContID, ParameterDirection.Input,
                    "@EmpanelmentID", SqlDbType.VarChar, objOTP.EmpanelmentID, ParameterDirection.Input,
                    "@expireTime", SqlDbType.VarChar, objOTP.expireTime, ParameterDirection.Input,
                    "@actionCode", SqlDbType.VarChar, objOTP.actionCode, ParameterDirection.Input,
                    "@response", SqlDbType.VarChar, objOTP.response, ParameterDirection.Input,
                    "@SMSmessage", SqlDbType.VarChar, objOTP.SMSmessage, ParameterDirection.Input,
                    "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;
        }
    }
}
