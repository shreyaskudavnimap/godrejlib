﻿using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class OfferNotificationDAL
    {   
        DbCommand _sqlcommand = null;

        DataAccess dataAccess = new DataAccess();

        public DataSet OfferNotificationData(mOfferNotification obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_OfferNotification", ref _sqlcommand,
                "@OfferNotificationID", SqlDbType.VarChar, obj.OfferNotificationID, ParameterDirection.Input,
                "@OfferID", SqlDbType.VarChar, obj.OfferID, ParameterDirection.Input,
                "@NotificationTitle", SqlDbType.VarChar, obj.NotificationTitle, ParameterDirection.Input,
                "@NotificationSubTilte", SqlDbType.VarChar, obj.NotificationSubTilte, ParameterDirection.Input,
                "@NotificationSendOn", SqlDbType.VarChar, obj.NotificationSendOn, ParameterDirection.Input,
                "@NotificationScheduledFor", SqlDbType.VarChar, obj.NotificationScheduledFor, ParameterDirection.Input,
                "@NotificationHasSend", SqlDbType.VarChar, obj.NotificationHasSend, ParameterDirection.Input,
                "@Active", SqlDbType.VarChar, obj.Active, ParameterDirection.Input,
                "@InsertedBy", SqlDbType.VarChar, obj.InsertedBy, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );

            return ds;
        }

    }
}