﻿using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Empanel
{
     public class ProjectNotificationDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        public DataSet ProjectNotificationData(mProjectNotification obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_ProjectNotification", ref _sqlcommand,
                "@ProjectNotificationID", SqlDbType.VarChar, obj.ProjectNotificationID, ParameterDirection.Input,
                "@ProjectID", SqlDbType.VarChar, obj.ProjectID, ParameterDirection.Input,
                "@NotificationTitle", SqlDbType.VarChar, obj.NotificationTitle, ParameterDirection.Input,
                "@NotificationSubTilte", SqlDbType.VarChar, obj.NotificationSubTilte, ParameterDirection.Input,
                "@NotificationSendOn", SqlDbType.VarChar, obj.NotificationSendOn, ParameterDirection.Input,
                "@NotificationScheduledFor", SqlDbType.VarChar, obj.NotificationScheduledFor, ParameterDirection.Input,
                "@NotificationHasSend", SqlDbType.VarChar, obj.NotificationHasSend, ParameterDirection.Input,
                "@Active", SqlDbType.VarChar, obj.Active, ParameterDirection.Input,
                "@InsertedBy", SqlDbType.VarChar, obj.InsertedBy, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );

            return ds;
        }


    }
}