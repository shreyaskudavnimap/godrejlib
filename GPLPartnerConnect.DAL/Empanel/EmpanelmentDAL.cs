﻿using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class EmpanelmentDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        public DataSet EmpanelmentData(mEmpanelment empl, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_Empanelment", ref _sqlcommand,
                "@EmplID", SqlDbType.VarChar, empl.EmplID, ParameterDirection.Input,
                "@company", SqlDbType.VarChar, empl.Company_Name, ParameterDirection.Input,
                "@entitytype", SqlDbType.VarChar, empl.entity_type, ParameterDirection.Input,
                "@FirstName", SqlDbType.VarChar, empl.FirstName, ParameterDirection.Input,
                "@LastName", SqlDbType.VarChar, empl.LastName, ParameterDirection.Input,
                "@mobile", SqlDbType.VarChar, empl.Mobile, ParameterDirection.Input,
                "@email", SqlDbType.VarChar, empl.Email, ParameterDirection.Input,
                "@commaddress", SqlDbType.VarChar, empl.communication_address, ParameterDirection.Input,
                "@regaddress", SqlDbType.VarChar, empl.registered_address, ParameterDirection.Input,
                "@panno", SqlDbType.VarChar, empl.Pan_No, ParameterDirection.Input,
                "@pancerti", SqlDbType.VarChar, empl.pancerti, ParameterDirection.Input,
                "@brokerid", SqlDbType.VarChar, empl.BrokerID, ParameterDirection.Input,
                "@Billing_City", SqlDbType.VarChar, empl.Billing_City, ParameterDirection.Input,
                "@Billing_Zip", SqlDbType.VarChar, empl.Billing_Zip, ParameterDirection.Input,
                "@Billing_Street", SqlDbType.VarChar, empl.Billing_Street, ParameterDirection.Input,
                "@Billing_State", SqlDbType.VarChar, empl.Billing_State, ParameterDirection.Input,
                "@Billing_Country", SqlDbType.VarChar, empl.Billing_Country, ParameterDirection.Input,
                "@Registered_City", SqlDbType.VarChar, empl.Registered_City, ParameterDirection.Input,
                "@Registered_Zip", SqlDbType.VarChar, empl.Registered_Zip, ParameterDirection.Input,
                "@Registered_Street", SqlDbType.VarChar, empl.Registered_Street, ParameterDirection.Input,
                "@Registered_State", SqlDbType.VarChar, empl.Registered_State, ParameterDirection.Input,
                "@Registered_Country", SqlDbType.VarChar, empl.Registered_Country, ParameterDirection.Input,
                "@Communication_City", SqlDbType.VarChar, empl.Communication_City, ParameterDirection.Input,
                "@Communication_Zip", SqlDbType.VarChar, empl.Communication_Zip, ParameterDirection.Input,
                "@Communication_Street", SqlDbType.VarChar, empl.Communication_Street, ParameterDirection.Input,
                "@Communication_State", SqlDbType.VarChar, empl.Communication_State, ParameterDirection.Input,
                "@Communication_Country", SqlDbType.VarChar, empl.Communication_Country, ParameterDirection.Input,
                "@Status", SqlDbType.VarChar, empl.status, ParameterDirection.Input,
                "@RequestType", SqlDbType.VarChar, empl.RequestType, ParameterDirection.Input,
                "@SubmittedByUser", SqlDbType.VarChar, empl.SubmittedByUser, ParameterDirection.Input,
                "@SourceCallCenterId", SqlDbType.VarChar, empl.SourceCallCenterId, ParameterDirection.Input,
                "@BrokerContactId", SqlDbType.VarChar, empl.BrokerContactID, ParameterDirection.Input,
                "@SourcedBy", SqlDbType.VarChar, empl.SourcedBy, ParameterDirection.Input,
                "@RelationshipManager", SqlDbType.VarChar, empl.RelationshipManager, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;

        }

        public DataSet EmpanelmentContactData(mEmpanelmentContact empl, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_EmpanelmentContact", ref _sqlcommand,
                "@EmplContID", SqlDbType.VarChar, empl.EmplContID, ParameterDirection.Input,
                "@Name", SqlDbType.VarChar, empl.Name, ParameterDirection.Input,
                "@Mobile", SqlDbType.VarChar, empl.Mobile, ParameterDirection.Input,
                "@Email", SqlDbType.VarChar, empl.Email, ParameterDirection.Input,
                "@Pan_No", SqlDbType.VarChar, empl.Pan_No, ParameterDirection.Input,
                "@Service_Tax_No", SqlDbType.VarChar, empl.Service_Tax_No, ParameterDirection.Input,
                "@TIN_No", SqlDbType.VarChar, empl.TIN_No, ParameterDirection.Input,
                "@Company_Name", SqlDbType.VarChar, empl.Company_Name, ParameterDirection.Input,
                "@Enterprise_Membership_ID", SqlDbType.VarChar, empl.Enterprise_Membership_ID, ParameterDirection.Input,
                "@Region", SqlDbType.VarChar, empl.Region, ParameterDirection.Input,
                "@BrokerID", SqlDbType.VarChar, empl.BrokerID, ParameterDirection.Input,
                "@SAP_Vendor_Code", SqlDbType.VarChar, empl.SAP_Vendor_Code, ParameterDirection.Input,
                "@username", SqlDbType.VarChar, empl.username, ParameterDirection.Input,
                "@password", SqlDbType.VarChar, empl.password, ParameterDirection.Input,
                "@createddate", SqlDbType.VarChar, empl.createddate, ParameterDirection.Input,
                "@Request_Pwd", SqlDbType.VarChar, empl.Request_Pwd, ParameterDirection.Input,
                "@Deviceid", SqlDbType.VarChar, empl.Deviceid, ParameterDirection.Input,
                "@platform", SqlDbType.VarChar, empl.platform, ParameterDirection.Input,
                "@mailsend", SqlDbType.VarChar, empl.mailsend, ParameterDirection.Input,
                "@pwdsend", SqlDbType.VarChar, empl.pwdsend, ParameterDirection.Input,
                "@ProfilePic", SqlDbType.VarChar, empl.ProfilePic, ParameterDirection.Input,
                "@BrokerAccountID", SqlDbType.VarChar, empl.BrokerAccountID, ParameterDirection.Input,
                "@BrokerContactID", SqlDbType.VarChar, empl.BrokerContactID, ParameterDirection.Input,
                "@Status", SqlDbType.VarChar, empl.Status, ParameterDirection.Input,
                "@PanCerti", SqlDbType.VarChar, empl.PanCerti, ParameterDirection.Input,
                "@EmpanelmentID", SqlDbType.VarChar, empl.EmpanelmentID, ParameterDirection.Input,
                "@Billing_City", SqlDbType.VarChar, empl.Billing_City, ParameterDirection.Input,
                "@Billing_Zip", SqlDbType.VarChar, empl.Billing_Zip, ParameterDirection.Input,
                "@Billing_Street", SqlDbType.VarChar, empl.Billing_Street, ParameterDirection.Input,
                "@Billing_State", SqlDbType.VarChar, empl.Billing_State, ParameterDirection.Input,
                "@Billing_Country", SqlDbType.VarChar, empl.Billing_Country, ParameterDirection.Input,
                "@Registered_City", SqlDbType.VarChar, empl.Registered_City, ParameterDirection.Input,
                "@Registered_Zip", SqlDbType.VarChar, empl.Registered_Zip, ParameterDirection.Input,
                "@Registered_Street", SqlDbType.VarChar, empl.Registered_Street, ParameterDirection.Input,
                "@Registered_State", SqlDbType.VarChar, empl.Registered_State, ParameterDirection.Input,
                "@Registered_Country", SqlDbType.VarChar, empl.Registered_Country, ParameterDirection.Input,
                "@Communication_City", SqlDbType.VarChar, empl.Communication_City, ParameterDirection.Input,
                "@Communication_Zip", SqlDbType.VarChar, empl.Communication_Zip, ParameterDirection.Input,
                "@Communication_Street", SqlDbType.VarChar, empl.Communication_Street, ParameterDirection.Input,
                "@Communication_State", SqlDbType.VarChar, empl.Communication_State, ParameterDirection.Input,
                "@Communication_Country", SqlDbType.VarChar, empl.Communication_Country, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;

        }

        public DataSet EmplGstDocsData(mGstDocs gst, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_EmplGstDocs", ref _sqlcommand,
               "@ID", SqlDbType.VarChar, gst.id, ParameterDirection.Input,
               "@EmplID", SqlDbType.VarChar, gst.emplid, ParameterDirection.Input,
               "@state", SqlDbType.VarChar, gst.state, ParameterDirection.Input,
               "@gstno", SqlDbType.VarChar, gst.gstno, ParameterDirection.Input,
               "@gstcerti", SqlDbType.VarChar, gst.gstcerti, ParameterDirection.Input,
               "@Comment", SqlDbType.VarChar, gst.comment, ParameterDirection.Input,
               "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;
        }

        public DataSet EmplReraDocsData(mReraDocs rera, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("USP_EmplReraDocs", ref _sqlcommand,
               "@ID", SqlDbType.VarChar, rera.id, ParameterDirection.Input,
               "@EmplID", SqlDbType.VarChar, rera.emplid, ParameterDirection.Input,
               "@state", SqlDbType.VarChar, rera.state, ParameterDirection.Input,
               "@rerano", SqlDbType.VarChar, rera.rerano, ParameterDirection.Input,
               "@reracerti", SqlDbType.VarChar, rera.reracerti, ParameterDirection.Input,
               "@Comment", SqlDbType.VarChar, rera.comment, ParameterDirection.Input,
               "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;
        }

        public DataSet GetEmplLoginDetails(mEmplLogin mlogin)
        {
            DataSet ds = dataAccess.GetDataSet("usp_GetEmplLogin", ref _sqlcommand,
               "@username", SqlDbType.VarChar, mlogin.username, ParameterDirection.Input,
               "@password", SqlDbType.VarChar, mlogin.password, ParameterDirection.Input);
            return ds;

        }

        public DataSet EmplContDeviceLogData(mEmplContDeviceLog empDev, String Flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_EmplContDeviceLog", ref _sqlcommand,
               "@EmplContDeviceLogID", SqlDbType.VarChar, empDev.EmplContDeviceLogID, ParameterDirection.Input,
               "@EmplContID", SqlDbType.VarChar, empDev.EmplContID, ParameterDirection.Input,
               "@DeviceToken", SqlDbType.VarChar, empDev.DeviceToken, ParameterDirection.Input,
               "@MobilePlatform", SqlDbType.VarChar, empDev.MobilePlatform, ParameterDirection.Input,
               "@Active", SqlDbType.VarChar, empDev.Active, ParameterDirection.Input,
               "@State", SqlDbType.VarChar, empDev.State, ParameterDirection.Input,
               "@City", SqlDbType.VarChar, empDev.City, ParameterDirection.Input,
               "@Flag", SqlDbType.VarChar, Flag, ParameterDirection.Input);
            return ds;

        }


        public DataSet EmplLogout(mEmplLogout mlogout, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_EmplLogout", ref _sqlcommand,
               "@EmplContDeviceLogID", SqlDbType.VarChar, mlogout.EmplContDeviceLogID, ParameterDirection.Input,
               "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);
            return ds;

        }
    }
}
