﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPLPartnerConnect.Model.Empanel;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class TrackerDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();
        //public DataSet PushTracker(mTracker obj, string flag)
        //{
        //    DataSet ds = dataAccess.GetDataSet("usp_AppTracker", ref _sqlcommand,
        //        "@UserName", SqlDbType.VarChar, obj.UserName, ParameterDirection.Input,
        //        "@Identifier", SqlDbType.VarChar, obj.Identifier, ParameterDirection.Input,
        //        "@NotificationId", SqlDbType.VarChar, obj.NotificationId, ParameterDirection.Input,
        //        "@JsonData", SqlDbType.VarChar, obj.JsonData, ParameterDirection.Input,
        //        "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
        //        );
        //    return ds;
        //}

        public DataSet PushNotificationTracker(mTracker obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_AppTracker", ref _sqlcommand,
                "@UserName", SqlDbType.VarChar, obj.UserName, ParameterDirection.Input,
                "@Identifier", SqlDbType.VarChar, obj.Identifier, ParameterDirection.Input,
                "@NotificationId", SqlDbType.VarChar, obj.NotificationId, ParameterDirection.Input,
                "@JsonData", SqlDbType.VarChar, obj.JsonData, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds;
        }

        public DataSet PushProjectTracker(mTracker obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_AppTracker", ref _sqlcommand,
                "@UserName", SqlDbType.VarChar, obj.UserName, ParameterDirection.Input,
                "@Identifier", SqlDbType.VarChar, obj.Identifier, ParameterDirection.Input,
                "@Module", SqlDbType.VarChar, obj.Module, ParameterDirection.Input,
                "@JsonData", SqlDbType.VarChar, obj.JsonData, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds;
        }
    }
}
