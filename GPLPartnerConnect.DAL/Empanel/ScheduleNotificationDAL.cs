﻿using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class ScheduleNotificationDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        public DataSet ScheduleNotificationData(mScheduleNotification obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_sc_ScheduleNotification", ref _sqlcommand,
                "@NotificationID", SqlDbType.VarChar, obj.NotificationID, ParameterDirection.Input,
                "@NotificationTitle", SqlDbType.VarChar, obj.NotificationTitle, ParameterDirection.Input,
                "@NotificationBody", SqlDbType.VarChar, obj.NotificationBody, ParameterDirection.Input,
                "@NotificationType", SqlDbType.VarChar, obj.NotificationType, ParameterDirection.Input,
                "@ChannelID", SqlDbType.VarChar, obj.ChannelID, ParameterDirection.Input,
                "@ChannelType", SqlDbType.VarChar, obj.ChannelType, ParameterDirection.Input,
                "@SubTitle", SqlDbType.VarChar, obj.SubTitle, ParameterDirection.Input,
                "@MobileUrl", SqlDbType.VarChar, obj.MobileUrl, ParameterDirection.Input,
                "@HasSend", SqlDbType.VarChar, obj.HasSend, ParameterDirection.Input,
                "@IsRedirect", SqlDbType.VarChar, obj.IsRedirect, ParameterDirection.Input,
                "@SentOn", SqlDbType.VarChar, obj.SentOn, ParameterDirection.Input,
                "@NotificationDate", SqlDbType.VarChar, obj.NotificationDate, ParameterDirection.Input,
                "@ScheduleDate", SqlDbType.VarChar, obj.ScheduleDate, ParameterDirection.Input,
                "@Active", SqlDbType.VarChar, obj.Active, ParameterDirection.Input,
                "@InsertedBy", SqlDbType.VarChar, obj.InsertedBy, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input,
                "@NotificationImage", SqlDbType.VarChar, obj.NotificationImage, ParameterDirection.Input);

            return ds;
        }

        public DataSet NotificationData(mNotification obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_Notification", ref _sqlcommand,
                "@NotificationID", SqlDbType.VarChar, obj.NotificationID, ParameterDirection.Input,
                "@NotificationTitle", SqlDbType.VarChar, obj.NotificationTitle, ParameterDirection.Input,
                "@NotificationBody", SqlDbType.VarChar, obj.NotificationBody, ParameterDirection.Input,
                "@SubTitle", SqlDbType.VarChar, obj.SubTitle, ParameterDirection.Input,
                "@ChannelType", SqlDbType.VarChar, obj.ChannelType, ParameterDirection.Input,
                "@NotificationSourceID", SqlDbType.VarChar, obj.NotificationSourceID, ParameterDirection.Input,
                "@NotificationType", SqlDbType.VarChar, obj.NotificationType, ParameterDirection.Input,
                "@MobileUrl", SqlDbType.VarChar, obj.MobileUrl, ParameterDirection.Input,
                "@ChannelID", SqlDbType.VarChar, obj.ChannelID, ParameterDirection.Input,
                "@IsRedirect", SqlDbType.VarChar, obj.IsRedirect, ParameterDirection.Input,
                "@HasSend", SqlDbType.VarChar, obj.HasSend, ParameterDirection.Input,
                "@SendOn", SqlDbType.VarChar, obj.SendOn, ParameterDirection.Input,
                "@ScheduledFor", SqlDbType.VarChar, obj.ScheduledFor, ParameterDirection.Input,
                "@Active", SqlDbType.VarChar, obj.Active, ParameterDirection.Input,
                "@InsertedBy", SqlDbType.VarChar, obj.InsertedBy, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input,
                "@State", SqlDbType.VarChar, obj.State, ParameterDirection.Input,
                "@City", SqlDbType.VarChar, obj.City, ParameterDirection.Input,
                "@NotificationImage", SqlDbType.VarChar, obj.NotificationImage, ParameterDirection.Input);

            return ds;
        }

        public DataSet DeleteNotificationByID(string notificationId, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_sc_ScheduleNotification", ref _sqlcommand,
               "@NotificationID", SqlDbType.VarChar, notificationId, ParameterDirection.Input,
               "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);

            return ds;
        }

        public DataSet GetTemplateById(int templateId)
        {
            DataSet ds = dataAccess.GetDataSet("usp_sc_GetTemplateById", ref _sqlcommand,
                "@TemplateId", SqlDbType.Int, templateId, ParameterDirection.Input);
            return ds;
        }

        //public DataSet NotificationLogData(mScheduleNotification obj, string flag)
        //{
        //    DataSet ds = dataAccess.GetDataSet("usp_ScheduleNotificationLog", ref _sqlcommand,
        //        "@NotificationLogID", SqlDbType.VarChar, obj.NotificationLogID, ParameterDirection.Input,
        //        "@NotificationID", SqlDbType.VarChar, obj.NotificationID, ParameterDirection.Input,
        //        "@EmplContDeviceLogID", SqlDbType.VarChar, obj.EmplContDeviceLogID, ParameterDirection.Input,
        //        "@EmplContID", SqlDbType.VarChar, obj.EmplContID, ParameterDirection.Input,
        //        "@DeviceToken", SqlDbType.VarChar, obj.DeviceToken, ParameterDirection.Input,
        //        "@NotificationTitle", SqlDbType.VarChar, obj.NotificationTitle, ParameterDirection.Input,
        //        "@NotificationBody", SqlDbType.VarChar, obj.NotificationBody, ParameterDirection.Input,
        //        "@NotificationSourceID", SqlDbType.VarChar, obj.NotificationSourceID, ParameterDirection.Input,
        //        "@NotificationType", SqlDbType.VarChar, obj.NotificationType, ParameterDirection.Input,
        //        "@ChannelType", SqlDbType.VarChar, obj.ChannelType, ParameterDirection.Input,
        //        "@IsRedirect", SqlDbType.VarChar, obj.IsRedirect, ParameterDirection.Input,
        //        "@MobileUrl", SqlDbType.VarChar, obj.MobileUrl, ParameterDirection.Input,
        //        "@ChannelID", SqlDbType.VarChar, obj.ChannelID, ParameterDirection.Input,
        //        "@SendStatus", SqlDbType.VarChar, obj.SendStatus, ParameterDirection.Input,
        //        "@ErrorMsg" , SqlDbType.VarChar, obj.ErrorMsg, ParameterDirection.Input, 
        //        "@Active", SqlDbType.VarChar, obj.Active, ParameterDirection.Input,
        //        "@InsertedBy", SqlDbType.VarChar, obj.InsertedBy, ParameterDirection.Input,
        //        "@SubTitle", SqlDbType.VarChar, obj.SubTitle, ParameterDirection.Input,
        //        "@MobilePlatform", SqlDbType.VarChar, obj.MobilePlatform, ParameterDirection.Input,
        //        "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);
        //    return ds;

        //}

    }
}
