﻿using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class EmplContDeviceDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        public DataSet GetEmplContDeviceId(string deviceToken)
        {
            DataSet ds = dataAccess.GetDataSet("usp_GetEmplContDeviceId", ref _sqlcommand,
                "@DeviceToken", SqlDbType.VarChar, deviceToken, ParameterDirection.Input);

            return ds;
        }
    }
}
