﻿using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class ProfileDataDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        public DataSet GetProfileData (mProfileData obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, obj.EmplId, ParameterDirection.Input,
                "@ContactNo", SqlDbType.VarChar, obj.ContactNo, ParameterDirection.Input,
                "@Email", SqlDbType.VarChar, obj.Email, ParameterDirection.Input,
                "@RegisteredAddress", SqlDbType.VarChar, obj.RegisteredAddress, ParameterDirection.Input,
                "@CommunicationAddress", SqlDbType.VarChar, obj.CommunicationAddress, ParameterDirection.Input,
                "@newContactNo", SqlDbType.VarChar, obj.newContactNo, ParameterDirection.Input,
                "@newEmail", SqlDbType.VarChar, obj.newEmail, ParameterDirection.Input,
                "@newRegisteredAddress", SqlDbType.VarChar, obj.newRegisteredAddress, ParameterDirection.Input,
                "@newCommunicationAddress", SqlDbType.VarChar, obj.newCommunicationAddress, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds;
        }

        public void InsertOTP(mContactOTP obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, obj.EmplId, ParameterDirection.Input,
                "@OTPNo", SqlDbType.VarChar, obj.OTPNo, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
        }

        public DataSet UpdateEmail(string emplContID, string newEmail, string input_user, string input_pass, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, emplContID, ParameterDirection.Input,
                "@newEmail", SqlDbType.VarChar, newEmail, ParameterDirection.Input,
                "@InputUser", SqlDbType.VarChar, input_user, ParameterDirection.Input,
                "@InputPass", SqlDbType.VarChar, input_pass, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );

            return ds;
        }

        public DataSet GetEmplReraGst(string emplContID, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, emplContID, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input);
            return ds;
        }

        public DataSet AddRera(string emplId, string rerano, string state, string reracerti)
        {
            DataSet ds = dataAccess.GetDataSet("usp_AddUpdateRera", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, emplId, ParameterDirection.Input,
                "@rerano", SqlDbType.VarChar, rerano, ParameterDirection.Input,
                "@state", SqlDbType.VarChar, state, ParameterDirection.Input,
                "@reracerti", SqlDbType.VarChar, reracerti, ParameterDirection.Input    );

            return ds;
        }

        public DataSet AddGst(string emplId, string gstno, string state, string gstcerti)
        {
            DataSet ds = dataAccess.GetDataSet("usp_AddUpdateGst", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, emplId, ParameterDirection.Input,
                "@gstno", SqlDbType.VarChar, gstno, ParameterDirection.Input,
                "@state", SqlDbType.VarChar, state, ParameterDirection.Input,
                "@gstcerti", SqlDbType.VarChar, gstcerti, ParameterDirection.Input);

            return ds;
        }

        public DataSet ValidateLogin(string emplContID, string input_user, string input_pass, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
               "@EmplId", SqlDbType.VarChar, emplContID, ParameterDirection.Input,
               "@InputUser", SqlDbType.VarChar, input_user, ParameterDirection.Input,
               "@InputPass", SqlDbType.VarChar, input_pass, ParameterDirection.Input,
               "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
               );

            return ds;
        }

        public DataSet UpdateRegAddress(string emplContID, string newRegAddress, string newRegCountry, string newRegState, string newRegCity, string newRegZipCode, string addrProof, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, emplContID, ParameterDirection.Input,
                "@newRegisteredAddress", SqlDbType.VarChar, newRegAddress, ParameterDirection.Input,
                "@newRegisteredCountry", SqlDbType.VarChar, newRegCountry, ParameterDirection.Input,
                "@newRegisteredState", SqlDbType.VarChar, newRegState, ParameterDirection.Input,
                "@newRegisteredCity", SqlDbType.VarChar, newRegCity, ParameterDirection.Input,
                "@newRegisteredZipCode", SqlDbType.VarChar, newRegZipCode, ParameterDirection.Input,
                "@AddressProof", SqlDbType.VarChar, addrProof, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );

            return ds;
        }

        public DataSet UpdateCommAddress(string emplContID, string newCommAddress, string newCommCity, string newCommState, string newCommZipPostal, string input_user, string input_pass, string flag, string newCommCountry)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, emplContID, ParameterDirection.Input,
                "@newCommunicationAddress", SqlDbType.VarChar, newCommAddress, ParameterDirection.Input,
                "@newCommunicationCity", SqlDbType.VarChar, newCommCity, ParameterDirection.Input,
                "@newCommunicationState", SqlDbType.VarChar, newCommState, ParameterDirection.Input,
                "@newCommunicationZipPostal", SqlDbType.VarChar, newCommZipPostal, ParameterDirection.Input,
                "@newCommunicationCountry", SqlDbType.VarChar, newCommCountry, ParameterDirection.Input,
                "@InputUser", SqlDbType.VarChar, input_user, ParameterDirection.Input,
                "@InputPass", SqlDbType.VarChar, input_pass, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );

            return ds;
        }

        public DataSet UpdateContact(string emplContID, string vertifyOtp, string newContactNo, string oldContact, string input_user, string input_pass, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, emplContID, ParameterDirection.Input,
                "@OTPNo", SqlDbType.VarChar, vertifyOtp, ParameterDirection.Input,
                "@newContactNo", SqlDbType.VarChar, newContactNo, ParameterDirection.Input,
                "@ContactNo", SqlDbType.VarChar, oldContact, ParameterDirection.Input,
                "@InputUser", SqlDbType.VarChar, input_user, ParameterDirection.Input,
                "@InputPass", SqlDbType.VarChar, input_pass, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );

            return ds;
        }

        public DataSet GetUserDetailsForAlert(string emplContID, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, emplContID, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds;
        }

        public DataSet VerifyOTP(string emplid, string otp, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, emplid, ParameterDirection.Input,
                "@OTP", SqlDbType.VarChar, otp, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );

            return ds;
        }
    }
}
