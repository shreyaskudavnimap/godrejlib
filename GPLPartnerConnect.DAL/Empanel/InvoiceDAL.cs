﻿using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class InvoiceDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        public DataSet GetInvoiceSummary (mInvoices obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_InvoiceSummary", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, obj.EmplId, ParameterDirection.Input,
                "@Quarters", SqlDbType.VarChar, obj.Quarters, ParameterDirection.Input,
                "@Region", SqlDbType.VarChar, obj.Region, ParameterDirection.Input,
                "@FiscalYearID", SqlDbType.VarChar, obj.FiscalYearID, ParameterDirection.Input,
                "@Project", SqlDbType.VarChar, obj.Project, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds;
        }

        public DataSet GetProfileData(mProfileData obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_UpdateProfile", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, obj.EmplId, ParameterDirection.Input,
                "@ContactNo", SqlDbType.VarChar, obj.ContactNo, ParameterDirection.Input,
                "@Email", SqlDbType.VarChar, obj.Email, ParameterDirection.Input,
                "@RegisteredAddress", SqlDbType.VarChar, obj.RegisteredAddress, ParameterDirection.Input,
                "@CommunicationAddress", SqlDbType.VarChar, obj.CommunicationAddress, ParameterDirection.Input,
                "@newContactNo", SqlDbType.VarChar, obj.newContactNo, ParameterDirection.Input,
                "@newEmail", SqlDbType.VarChar, obj.newEmail, ParameterDirection.Input,
                "@newRegisteredAddress", SqlDbType.VarChar, obj.newRegisteredAddress, ParameterDirection.Input,
                "@newCommunicationAddress", SqlDbType.VarChar, obj.newCommunicationAddress, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds;
            
        }

        public DataSet InvoiceData(mInvoiceSummaryDetail obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_InvoiceSummary", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, obj.EmplId, ParameterDirection.Input,
                "@Quarters", SqlDbType.VarChar, obj.Quarters, ParameterDirection.Input,
                "@Location", SqlDbType.VarChar, obj.Location, ParameterDirection.Input,
                "@FiscalYearID", SqlDbType.VarChar, obj.FiscalYearID, ParameterDirection.Input,
                "@ProjectId", SqlDbType.VarChar, obj.ProjectId, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input,
                "@Year", SqlDbType.VarChar, obj.Year, ParameterDirection.Input
                );
            return ds;
        }

        public DataSet InvoiceSummaryData(mInvoiceSummaryDetail obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_InvoiceSummary", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, obj.EmplId, ParameterDirection.Input,
                "@Quarters", SqlDbType.VarChar, obj.Quarters, ParameterDirection.Input,
                "@Location", SqlDbType.VarChar, obj.Location, ParameterDirection.Input,
                "@Year", SqlDbType.VarChar, obj.Year, ParameterDirection.Input,
                "@FiscalYearID", SqlDbType.VarChar, obj.FiscalYearID, ParameterDirection.Input,
                "@ProjectId", SqlDbType.VarChar, obj.ProjectId, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds;
        }

        public DataSet InvoiceExcelExport(mInvoiceSummaryDetail obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_InvoiceSummary", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, obj.EmplId, ParameterDirection.Input,
                "@FiscalYearID", SqlDbType.VarChar, obj.FiscalYearID, ParameterDirection.Input,
                "@ProjectId", SqlDbType.VarChar, obj.ProjectId, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds;
        }
    }
}
