﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPLPartnerConnect.Model.Empanel;

namespace GPLPartnerConnect.DAL.Empanel
{
    public class EOIDAL
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();
        public DataSet InsertOTP(mEOI obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_EOI", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, obj.EmplId, ParameterDirection.Input,
                "@Mobile", SqlDbType.VarChar, obj.Mobile, ParameterDirection.Input,
                "@OTPNo", SqlDbType.VarChar, obj.OTPNo, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds;
        }

        public string ValidateOTP(mEOI obj, string flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_EOI", ref _sqlcommand,
                "@EmplId", SqlDbType.VarChar, obj.EmplId, ParameterDirection.Input,
                "@Mobile", SqlDbType.VarChar, obj.Mobile, ParameterDirection.Input,
                "@OTPNo", SqlDbType.VarChar, obj.OTPNo, ParameterDirection.Input,
                "@flag", SqlDbType.VarChar, flag, ParameterDirection.Input
                );
            return ds.Tables[0].Rows[0]["Status"].ToString();
        }
    }
}
