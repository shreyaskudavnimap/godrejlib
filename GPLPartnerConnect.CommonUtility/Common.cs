﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace GPLPartnerConnect.CommonUtility
{
    public static class Common
    {

        public static string JSONSerialize(object objToSerialize)
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new StringEnumConverter());

            var serializer = JsonSerializer.Create(settings);

            var sb = new StringBuilder();
            using (var sw = new StringWriter(sb))
            {
                serializer.Serialize(sw, objToSerialize);
            }

            return sb.ToString();
        }

        public static T JSONDeSerialize<T>(string json)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = 500000000;
            return js.Deserialize<T>(json);
        }


        public static List<T> ToObject<T>(this DataTable dt) where T : new()
        {
            List<T> list = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = ToObject<T>(dt, row);

                list.Add(item);
            }

            return list;
        }

        public static T ToObject<T>(DataTable dt, DataRow row) where T : new()
        {
            T item = new T();
            foreach (DataColumn column in dt.Columns)
            {
                PropertyInfo property = item.GetType().GetProperty(column.ColumnName);

                if (property != null && row[column] != DBNull.Value)
                {
                    object result = Convert.ChangeType(row[column].ToString(), property.PropertyType);
                    property.SetValue(item, result, null);
                }
            }
            return item;
        }
    }
}
