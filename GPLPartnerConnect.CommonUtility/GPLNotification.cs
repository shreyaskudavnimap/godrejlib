﻿using GPLPartnerConnect.Model.FCM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace GPLPartnerConnect.CommonUtility
{
    public sealed class GPLNotification
    {
        private string _FCMUrl = "https://fcm.googleapis.com/fcm/send";
        private string _SenderId = "74240988336";
        private string _ServerKey = ConfigurationManager.AppSettings["FireBaseServerKey"].ToString();

        private static GPLNotification instance = null;
        private static readonly object padlock = new object();

        GPLNotification()
        {
        }

        public static GPLNotification Instance
        {
            get
            {
                lock (padlock) { return instance == null ? new GPLNotification() : instance; }
            }
        }

        public mRespon SendNotification(string deviceToken, string title, string msg)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(_FCMUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", _ServerKey));
            httpWebRequest.Headers.Add(string.Format("Sender: id={0}", _SenderId));
            httpWebRequest.Method = "POST";
            var payload = new
            {
                to = deviceToken,
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = msg,
                    title = title
                },
            };
            var serializer = new JavaScriptSerializer();
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = serializer.Serialize(payload);
                streamWriter.Write(json);
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                mRespon fcmResp = Common.JSONDeSerialize<mRespon>(responseText);
                return fcmResp;
            }
        }
        public mRespon SendNotification(Model.FCM.FcmMessage payload)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(_FCMUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", _ServerKey));
            httpWebRequest.Headers.Add(string.Format("Sender: id={0}", _SenderId));
            httpWebRequest.Method = "POST";
            var serializer = new JavaScriptSerializer();
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = serializer.Serialize(payload);
                streamWriter.Write(json);
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                mRespon fcmResp = Common.JSONDeSerialize<mRespon>(responseText);
                return fcmResp;
            }
        }

        //public mRespon SendNotificationIos(Model.FCM.mMsgIos payload)
        //{
        //    var httpWebRequest = (HttpWebRequest)WebRequest.Create(_FCMUrl);
        //    httpWebRequest.ContentType = "application/json";
        //    httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", _ServerKey));
        //    httpWebRequest.Headers.Add(string.Format("Sender: id={0}", _SenderId));
        //    httpWebRequest.Method = "POST";
        //    var serializer = new JavaScriptSerializer();
        //    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //    {
        //        string json = serializer.Serialize(payload);
        //        streamWriter.Write(json);
        //        streamWriter.Flush();
        //    }
        //    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        //    {
        //        var responseText = streamReader.ReadToEnd();
        //        mRespon fcmResp = Common.JSONDeSerialize<mRespon>(responseText);
        //        return fcmResp;
        //    }
        //}
    }
}
