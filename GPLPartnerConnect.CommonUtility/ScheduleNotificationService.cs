﻿using GPLPartnerConnect.DAL;
using GPLPartnerConnect.Model.Empanel;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.CommonUtility
{
    class ScheduleNotificationService
    {
        DbCommand _sqlcommand = null;
        DataAccess dataAccess = new DataAccess();

        //(string recurringJobId, Expression<Func<Task>> methodCall, string cronExpression, TimeZoneInfo timeZone = null, string queue = "default");
        string recurringJobId = "1";

        public DataTable dt = new DataTable();
        
        
        public void SendNotification(DataTable dt, string userid)
        {
            int cnt = 0, totaldevicecnt = 0;
            mNotificationLog obj = new mNotificationLog();
            foreach (DataRow row in dt.Rows)
            {
                obj.NotificationID = row["NotificationID"].ToString();
                obj.NotificationTitle = row["NotificationTitle"].ToString();
                obj.NotificationBody = row["NotificationBody"].ToString();
                obj.NotificationSourceID = row["NotificationSourceID"].ToString();
                obj.NotificationType = row["NotificationType"].ToString();
                obj.ChannelType = row["ChannelType"].ToString();
                obj.MobileUrl = row["MobileUrl"].ToString();
                obj.ChannelID = row["ChannelID"].ToString();
                obj.IsRedirect = row["IsRedirect"].ToString();
                obj.InsertedBy = userid;
                obj.Active = "Y";
                obj.SubTitle = row["SubTitle"].ToString();
                obj.NotificationImage = row["NotificationImage"].ToString();

                DataSet dsEmp = GetActiveDevice();
                totaldevicecnt += dsEmp.Tables[0].Rows.Count;
                foreach (DataRow rowEmp in dsEmp.Tables[0].Rows)
                {
                    obj.SendStatus = "";
                    obj.ErrorMsg = "";
                    obj.EmplContDeviceLogID = rowEmp["EmplContDeviceLogID"].ToString();
                    obj.EmplContID = rowEmp["EmplContID"].ToString();
                    obj.DeviceToken = rowEmp["DeviceToken"].ToString();
                    obj.MobilePlatform = rowEmp["MobilePlatform"].ToString();

                    //obj.NotificationLogID = InsertNotificationLog(obj).ToString();

                    Model.FCM.mMsg fcmmsg = new Model.FCM.mMsg();
                    fcmmsg.to = obj.DeviceToken;
                    fcmmsg.data.title = obj.NotificationTitle;
                    fcmmsg.data.body = obj.SubTitle;
                    fcmmsg.data.ChannelType = obj.ChannelType;
                    fcmmsg.data.id = obj.ChannelID;
                    fcmmsg.data.NotificationLogID = obj.NotificationLogID;

                    Model.FCM.mRespon fcmResp = GPLNotification.Instance.SendNotification(fcmmsg);



                    cnt += Convert.ToInt16(fcmResp.success);
                    if (fcmResp.success == "0")
                    {
                        obj.SendStatus = "N";
                        obj.ErrorMsg = fcmResp.results[0].error;
                    }
                    else
                    {
                        obj.SendStatus = "Y";
                    }

                    //UpdateNotificationLog(obj);
                }
                //UpdateNotificationSendStatus(obj.NotificationID);
            }
            //return cnt + " of " + totaldevicecnt;
        }

        public DataSet GetActiveDevice()
        {
            mEmplContDeviceLog mobj = new mEmplContDeviceLog();
            return EmplContDeviceLogData(mobj, "D");
        }

        public DataSet EmplContDeviceLogData(mEmplContDeviceLog empDev, String Flag)
        {
            DataSet ds = dataAccess.GetDataSet("usp_EmplContDeviceLog", ref _sqlcommand,
               "@EmplContDeviceLogID", SqlDbType.VarChar, empDev.EmplContDeviceLogID, ParameterDirection.Input,
               "@EmplContID", SqlDbType.VarChar, empDev.EmplContID, ParameterDirection.Input,
               "@DeviceToken", SqlDbType.VarChar, empDev.DeviceToken, ParameterDirection.Input,
               "@MobilePlatform", SqlDbType.VarChar, empDev.MobilePlatform, ParameterDirection.Input,
               "@Active", SqlDbType.VarChar, empDev.Active, ParameterDirection.Input,
               "@Flag", SqlDbType.VarChar, Flag, ParameterDirection.Input);
            return ds;

        }
    }

}
