﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mReraDocs
    {
        public string id { get; set; }
        public string emplid { get; set; }
        public string state { get; set; }
        public string rerano { get; set; }
        public string reracerti { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
        public string sfdcregisted { get; set; }
        public bool insertflag { get; set; }
    }
}
