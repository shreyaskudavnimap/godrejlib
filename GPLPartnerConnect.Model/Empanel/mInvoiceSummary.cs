﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mInvoiceSummary
    {
        
        public string EmplId { get; set; }
        public string Quarters { get; set; }
        public string FiscalYearID { get; set; }
        public string Project { get; set; }
        public string Location { get; set; }
        public string TotalPaid { get; set; }
        public string InvoiceInProgress { get; set; }
        public string TotalBilled { get; set; }

        public List<mInvoiceSummaryDetail> detail { get; set; }

        public mInvoiceSummary()
        {
            detail = new List<mInvoiceSummaryDetail>();
        }
    }
}
