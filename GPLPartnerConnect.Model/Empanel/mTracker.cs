﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mTracker
    {
        public string EmplID { get; set; }
        public string JsonData { get; set; }
        public string Identifier { get; set; }
        public string Module { get; set; }
        public string UserName { get; set; }
        public string NotificationId { get; set; }
    }
}
