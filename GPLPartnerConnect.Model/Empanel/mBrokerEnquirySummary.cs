﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mBrokerEnquirySummary
    {
        public string EmplID { get; set; }
        public string EmplContID { get; set; }
        public string Location { get; set; }
        public string EnquiryCount { get; set; }
        public List<mBrokerEnquirySummaryDetail> detail { get; set; }
        
        public mBrokerEnquirySummary()
        {
            detail = new List<mBrokerEnquirySummaryDetail>();
        }
    }
}
