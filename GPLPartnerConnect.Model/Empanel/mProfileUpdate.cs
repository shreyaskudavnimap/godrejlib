﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mReraUpdate
    {
        public string UserId { get; set; }

        public List<mReraDocs> list = new List<mReraDocs>();

    }

    public class mGstUpdate
    {
        public string UserId { get; set; }

        public List<mGstDocs> list = new List<mGstDocs>();

    }

}
