﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mEmplContDeviceLog
    {
        public string EmplContDeviceLogID { set; get; }
        public string EmplContID { set; get; } 
        public string DeviceToken { set; get; }
        public string MobilePlatform { set; get; }
        public string LastAccessed { set; get; }
        public string Active { set; get; }
        public string State { get; set; }
        public string City { get; set; }
    }
}
