﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mProfileData
    {
        public string EmplId { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string RegisteredCountry { get; set; }
        public string CommunicationCountry { get; set; }
        public string RegisteredAddress { get; set; }
        public string CommunicationAddress { get; set; }
        public string RegisteredCity { get; set; }
        public string RegisteredState { get; set; }
        public string RegisteredZip { get; set; }
        public string CommunicationCity { get; set; }
        public string CommunicationState { get; set; }
        public string CommunicationZip { get; set; }
        public string newContactNo { get; set; }
        public string newEmail { get; set; }
        public string newRegisteredAddress { get; set; }
        public string newCommunicationAddress { get; set; }
        public string flag { get; set; }

    }

    public class mContactOTP
    {
        public string OTPNo { get; set; }

        public string EmplId { get; set; }
    }
}
