﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mEmpanelmentContact
    {
        public string EmplContID { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Pan_No { get; set; }
        public string Service_Tax_No { get; set; }
        public string TIN_No { get; set; }
        public string Company_Name { get; set; }
        public string Enterprise_Membership_ID { get; set; }
        public string Region { get; set; }
        public string BrokerID { get; set; }
        public string SAP_Vendor_Code { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string createddate { get; set; }
        public string Request_Pwd { get; set; }
        public string Deviceid { get; set; }
        public string platform { get; set; }
        public string mailsend { get; set; }
        public string pwdsend { get; set; }
        public string ProfilePic { get; set; }
        public string BrokerAccountID { get; set; }
        public string BrokerContactID { get; set; }
        public string Status { get; set; }
        public string PanCerti { get; set; }
        public string EmpanelmentID { get; set; }
        public string Billing_City { get; set; }
        public string Billing_Zip { get; set; }
        public string Billing_Street { get; set; }
        public string Billing_State { get; set; }
        public string Billing_Country { get; set; }
        public string Registered_City { get; set; }
        public string Registered_Zip { get; set; }
        public string Registered_Street { get; set; }
        public string Registered_State { get; set; }
        public string Registered_Country { get; set; }
        public string Communication_City { get; set; }
        public string Communication_Zip { get; set; }
        public string Communication_Street { get; set; }
        public string Communication_State { get; set; }
        public string Communication_Country { get; set; }

    }
}
