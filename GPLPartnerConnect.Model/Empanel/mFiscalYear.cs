﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mFiscalYear
    {
        public string FiscalYearID { get; set; }
        public string FiscalYearName { get; set; }
        public string FromDate { get; set; }
        public string Todate { get; set; }
        public List<mQuarter> Quarters { get; set; }
    }
}
