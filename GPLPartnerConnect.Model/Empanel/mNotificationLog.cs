﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mNotificationLog
    {
        public string NotificationLogID { get; set; }
        public string NotificationID { get; set; }
        public string EmplContDeviceLogID { get; set; }
        public string EmplContID { get; set; }
        public string DeviceToken { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationSubTitle { get; set; }
        public string NotificationBody { get; set; }
        public string NotificationSourceID { get; set; }
        public string NotificationType { get; set; }
        public string ChannelType { get; set; }
        public string MobileUrl { get; set; }
        public string ChannelID { get; set; }
        public string SendStatus { get; set; }
        public string ErrorMsg { get; set; }
        public string IsRedirect { get; set; }
        public string InsertedBy { get; set; }
        public string Active { get; set; }
        public string SubTitle { get; set; }
        public string MobilePlatform { get; set; }
        public string NotificationImage { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }
    }
}
