﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mBrokerEnquiry
    {
        public string EmplID { get; set; }
        public string EmplContID { get; set; }
        public string SFDC_id { get; set; }
        public string SFDC_Name { get; set; }
        public string SFDC_ContactNameC { get; set; }
        public string SFDC_PropStrengthProjectC { get; set; }
        public string SFDC_MobileNumberC { get; set; }
        public string SFDC_PropStrengthRequestStatusC { get; set; }
        public string SFDC_OldCommentsC { get; set; }
        public string SFDC_MarketingProjectNameC { get; set; }
        public string SFDC_PropStrengthBrokerAccountC { get; set; }
        public string SFDC_PropStrengthBrokerContactC { get; set; }
        public string SFDC_DateOfSiteVisitC { get; set; }
        public string SFDC_lastmodifieddate { get; set; }
        public string SFDC_DateOfEnquiry1C { get; set; }
        public string SFDC_BrokerPANNoC { get; set; }
        public string SFDC_PanNumber { get; set; }
        public string SFDC_Email { get; set; }
        public string SFDC_Region { get; set; }
        public string title { get; set; }
    }
    public class mBrokerEnquiryMoblie
    {
        public string EmplID { get; set; }
        public string EmplContID { get; set; }
        public string ID { get; set; }
        public string customerName { get; set; }
        public string ProjectName { get; set; }
        public string mobileNo { get; set; }
        public string comments { get; set; }
        public string enquiryStatus { get; set; }
        
    }
    public class mBrokerEnquirySFDC
    {
        public string id { get; set; }
        public string Name { get; set; }
        public string Contact_name__c { get; set; }
        public string PropStrength__Project__c { get; set; }
        public string Mobile_number__c { get; set; }
        public string propstrength__request_status__c { get; set; }
        public string old_comments__c { get; set; }
        public string marketing_project_name__c { get; set; }
        public string PropStrength__Broker_Account__c { get; set; }
        public string PropStrength__Broker_Contact__c { get; set; }
        public string date_of_site_visit__c { get; set; }
        public string lastmodifieddate { get; set; }
        public string date_of_enquiry1__c { get; set; }
        public string Broker_PAN_No__c { get; set; }
        public string PanNumber { get; set; }
        public string Email { get; set; }
        public string region { get; set; }
        public mBrokerEnquiry ToBrokerEnquiry()
        {
            mBrokerEnquiry obj = new mBrokerEnquiry();
            obj.SFDC_id = id;
            obj.SFDC_ContactNameC = this.Contact_name__c;
            obj.SFDC_PropStrengthProjectC = this.PropStrength__Project__c;
            obj.SFDC_MobileNumberC = this.Mobile_number__c;
            obj.SFDC_PropStrengthRequestStatusC = this.propstrength__request_status__c;
            obj.SFDC_OldCommentsC = this.old_comments__c;
            obj.SFDC_MarketingProjectNameC = this.marketing_project_name__c;
            obj.SFDC_PropStrengthBrokerAccountC = this.PropStrength__Broker_Account__c;
            obj.SFDC_PropStrengthBrokerContactC = this.PropStrength__Broker_Contact__c;
            obj.SFDC_BrokerPANNoC = this.Broker_PAN_No__c;
            obj.SFDC_DateOfSiteVisitC = this.date_of_site_visit__c;
            obj.SFDC_Name = this.Name;
            obj.SFDC_lastmodifieddate = this.lastmodifieddate;
            obj.SFDC_DateOfEnquiry1C = this.date_of_enquiry1__c;
            obj.SFDC_PanNumber = this.PanNumber;
            obj.SFDC_Email = this.Email;
            obj.SFDC_Region = this.region;
            return obj;
        }
    }
}
