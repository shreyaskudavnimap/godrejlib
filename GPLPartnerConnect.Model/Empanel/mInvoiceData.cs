﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mInvoiceData
    {
        public string WorkorderId { get; set; }
        public string CustomerName { get; set; }
        public string WOAmount { get; set; }
        public string TotalPaid { get; set; }

    }
}
