﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mScheduleNotification
    {
        public string NotificationID {get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationBody { get; set; }
        public string NotificationDate { get; set; }
        public string NotificationType { get; set; }
        public string MobileUrl { get; set; }
        public string InsertedOn { get; set; }
        public string InsertedBy { get; set; }
        public string Active { get; set; }
        public string HasSend { get; set; }
        public string SentOn { get; set; }
        public string NotificationImage { get; set; }

        public string SubTitle { get; set; }
        public string IsRedirect { get; set; }
        public string ChannelType { get; set; }
        public string ChannelID { get; set; }

        public string ScheduleDate { get; set;  }

    }
}
