﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mGstDocs
    {
        public string id { get; set; }
        public string emplid { get; set; }
        public string state { get; set; }
        public string gstno { get; set; }
        public string gstcerti { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
        public string sfdcregisted { get; set; }
        public bool insertflag { get; set; }
    }
}
