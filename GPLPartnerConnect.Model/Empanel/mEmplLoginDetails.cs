﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mEmplLoginDetails
    {
        public string EmplContID { get; set; }
        public string EmpanelmentID { get; set; }
        public string BrokerID { get; set; }
        public string Name { get; set; }
        public string BrokerAccountID { get; set; }
        public string Email { get; set; }
        public string PanNo { get; set; }
        public string Mobile { get; set; }
        public string BrokerContactID { get; set; }
        public string Status { get; set; }
        public string CompanyName { get; set; }
        public string isSentOTP { get; set; }
        public string region { get; set; }


    }
}
