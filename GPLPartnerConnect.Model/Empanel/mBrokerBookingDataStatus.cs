﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mBrokerBookingDataStatus
    {
        public string EmplID { get; set; }
        public string EmplContID { get; set; }
        public string DataCount { get; set; }
        public string LastSync { get; set; }
    }
}
