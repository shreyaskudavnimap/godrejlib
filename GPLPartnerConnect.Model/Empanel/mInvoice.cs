﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mInvoices
    {
        public string FiscalYearID { get; set; }

        public string EmplId { get; set; }

        public string Quarters { get; set; }
        public string Region { get; set; }
        public string Project { get; set; }
    }
}
