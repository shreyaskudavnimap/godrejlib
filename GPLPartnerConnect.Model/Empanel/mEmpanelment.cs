﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mEmpanelment
    {
        public string EmplID { get; set; }
        public string Company_Name { get; set; }
        public string entity_type { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string communication_address { get; set; }
        public string registered_address { get; set; }
        public string Pan_No { get; set; }
        public string pancerti { get; set; }


        public string BrokerID
        { get; set; }


        public string BrokerContactID
        { get; set; }


        public List<mReraDocs> ReraDoc
        { get; set; }


        public List<mGstDocs> GstDoc
        { get; set; }


        public string status
        { get; set; }


        public string Billing_City
        { get; set; }


        public string Billing_Zip
        { get; set; }


        public string Billing_Street
        { get; set; }


        public string Billing_State
        { get; set; }


        public string Billing_Country
        { get; set; }


        public string Registered_City
        { get; set; }


        public string Registered_Zip
        { get; set; }


        public string Registered_Street
        { get; set; }


        public string Registered_State
        { get; set; }


        public string Registered_Country
        { get; set; }


        public string Communication_City
        { get; set; }


        public string Communication_Zip
        { get; set; }


        public string Communication_Street
        { get; set; }


        public string Communication_State
        { get; set; }


        public string Communication_Country
        { get; set; }
        public string SubmittedByUser { get; set; }     // Role_dev
        public string RequestType { get; set; }         // Role_dev
        public string SourceCallCenterId { get; set; }
        public string SourcedBy { get; set; }
        public string RelationshipManager { get; set; }

        public mEmpanelment()
        {
            GstDoc = new List<mGstDocs>();
            ReraDoc = new List<mReraDocs>();
        }
    }
}
