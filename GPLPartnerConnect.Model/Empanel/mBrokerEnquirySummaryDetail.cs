﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mBrokerEnquirySummaryDetail
    {
        public string EmplID { get; set; }
        public string EmplContID { get; set; }
        public string Location { get; set; }
        public string ProjectName { get; set; }
        public string EnquiryCount { get; set; }
        public string SFDCProjectID { get; set; }
    }
}
