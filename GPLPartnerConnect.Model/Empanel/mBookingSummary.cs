﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mBookingSummary
    {
        public string EmplID { get; set; }
        public string EmplContID { get; set; }
        public string Location { get; set; }
        public string BookingCount { get; set; }
        public string NetBV { get; set; }
        public List<mBookingSummaryDetail> detail { get; set; }
        
        public mBookingSummary()
        {
            detail = new List<mBookingSummaryDetail>();
        }
    }
}
