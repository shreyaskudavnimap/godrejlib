﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mOfferNotification
    {
        public string OfferNotificationID { get; set; }
        public string OfferID { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationSubTilte { get; set; }
        public string NotificationSendOn { get; set; }
        public string NotificationScheduledFor { get; set; }
        public string NotificationHasSend { get; set; }
        public string InsertedBy { get; set; }
        public string Active { get; set; }

    }
}