﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mEmplLogout
    {
        public string EmplContID { get; set; }
        public string EmplContDeviceLogID { get; set; }
    }
}
