﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mBrokerBooking
    {
        public string EmplID { get; set; }
        public string EmplContID { get; set; }
        public string SFDC_id { get; set; }
        public string SFDC_PropStrengthPrimaryApplicantNameC { get; set; }
        public string SFDC_PropStrengthProjectC { get; set; }
        public string SFDC_ProjectNameC { get; set; }
        public string SFDC_PropStrengthBookingDateC { get; set; }
        public string SFDC_PropStrengthRevisedAgreementAmountC { get; set; }
        public string SFDC_RegistrationDateC { get; set; }
        public string SFDC_PropStrengthBrokerAccountC { get; set; }
        public string SFDC_PropStrengthBrokerContactC { get; set; }
        public string SFDC_BrokerPANNoC { get; set; }
        public string SFDC_TotalPaymentRealizedwrtTAVC { get; set; }
        public string SFDC_name { get; set; }
        public string SFDC_lastmodifieddate { get; set; }
        public string SFDC_PropstrengthPropertyNameC { get; set; }
        public string SFDC_SAPCustomerCreatedDateC { get; set; }
        public string SFDC_Region { get; set; }
        public string SFDC_house_unit { get; set; }
        public string SFDC_tower_code { get; set; }
        public string SFDC_Wing_Block { get; set; }
        public string SFDC_tower_name { get; set; }
        public string SFDC_cip_tower { get; internal set; }
        public string application_booking_18 { get; set; }
        public string booking_status { get; set; }
        public string PropStrength__Booking_Cancelled_Date__c { get; set; }

        public string B1P__c { get; set; }
        public string B2P__c { get; set; }
        public string B3P__c { get; set; }
        public string B4P__c { get; set; }
        public string B5P__c { get; set; }
        public string B6P__c { get; set; }
        public string B7P__c { get; set; }
        public string TBP__c { get; set; }
        public string PropStrength__Brokerage_Amount__c { get; set; }
        public string title { get; set; }
        public string propstrength__total_agreement_amount__c { get; internal set; }
    }

    public class mBrokerBookingMoblie
    {
        public string EmplID { get; set; }
        public string EmplContID { get; set; }
        public string PrimaryApplicantName { get; set; }
        public string ProjectName { get; set; }
        public string RegistrationDate { get; set; }
        public string AgreementAmount { get; set; }
        public string paymentRealized { get; set; }
        public string brokerPANNo { get; set; }
    }

    public class mBrokerBookingSFDC
    {
        public string id { get; set; }
        public string PropStrength__Primary_Applicant_Name__c { get; set; }
        public string PropStrength__Project__c { get; set; }
        public string Project_Name__c { get; set; }
        public string PropStrength__Booking_Date__c { get; set; }
        public string PropStrength__Revised_Agreement_Amount__c { get; set; }
        public string Registration_Date__c { get; set; }
        public string PropStrength__Broker_Account__c { get; set; }
        public string PropStrength__Broker_Contact__c { get; set; }
        public string Broker_PAN_No__c { get; set; }
        public string Total_Payment_Realized_wrt_TAV__c { get; set; }
        public string name { get; set; }
        public string lastmodifieddate { get; set; }
        public string propstrength__property_name__c { get; set; }

        public string SAP_Customer_Created_Date__c { get; set; }

        public string region { get; set; }

        public string house_unit { get; set; }      // Shreyas 13-07-2020 FlatNo_issue
        public string tower_code { get; set; }      // Shreyas 13-07-2020 FlatNo_issue
        public string Wing_Block { get; set; }      // Shreyas 13-07-2020 FlatNo_issue
        public string tower_name { get; set; }  // Shreyas 15-07-2020 FlatNo_issue
        public string cip_tower { get; set; }
        public string application_booking_18 { get; set; }
        public string booking_status { get; set; }
        public string PropStrength__Booking_Cancelled_Date__c { get; set; }
        public string B1P__c { get; set; }
        public string B2P__c { get; set; }
        public string B3P__c { get; set; }
        public string B4P__c { get; set; }
        public string B5P__c { get; set; }
        public string B6P__c { get; set; }
        public string B7P__c { get; set; }
        public string TBP__c { get; set; }
        public string PropStrength__Brokerage_Amount__c { get; set; }
        public string propstrength__total_agreement_amount__c { get; set; }

        public mBrokerBooking ToBrokerBooking()
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.SFDC_id = id;
            obj.SFDC_PropStrengthPrimaryApplicantNameC = this.PropStrength__Primary_Applicant_Name__c;
            obj.SFDC_PropStrengthProjectC = this.PropStrength__Project__c;
            obj.SFDC_ProjectNameC = this.Project_Name__c;
            obj.SFDC_PropStrengthBookingDateC = this.PropStrength__Booking_Date__c == "" ? null : this.PropStrength__Booking_Date__c;
            obj.SFDC_PropStrengthRevisedAgreementAmountC = this.PropStrength__Revised_Agreement_Amount__c;
            obj.SFDC_RegistrationDateC = this.Registration_Date__c == "" ? null : this.Registration_Date__c;
            obj.SFDC_PropStrengthBrokerAccountC = this.PropStrength__Broker_Account__c;
            obj.SFDC_PropStrengthBrokerContactC = this.PropStrength__Broker_Contact__c;
            obj.SFDC_BrokerPANNoC = this.Broker_PAN_No__c;
            obj.SFDC_TotalPaymentRealizedwrtTAVC = this.Total_Payment_Realized_wrt_TAV__c;
            obj.SFDC_name = this.name;
            obj.SFDC_lastmodifieddate = this.lastmodifieddate == "" ? null : this.lastmodifieddate;
            obj.SFDC_PropstrengthPropertyNameC = this.propstrength__property_name__c;
            obj.SFDC_SAPCustomerCreatedDateC = this.SAP_Customer_Created_Date__c == "" ? null : this.SAP_Customer_Created_Date__c;
            obj.SFDC_Region = this.region;
            obj.SFDC_house_unit = this.house_unit == "" ? null : this.house_unit;   // Shreyas 13-07-2020 FlatNo_issue
            obj.SFDC_tower_code = this.tower_code == "" ? null : this.tower_code;   // Shreyas 13-07-2020 FlatNo_issue
            obj.SFDC_Wing_Block = this.Wing_Block == "" ? null : this.Wing_Block;   // Shreyas 13-07-2020 FlatNo_issue
            obj.SFDC_tower_name = this.tower_name == "" ? null : this.tower_name;   // Shreyas 15-07-2020 FlatNo_issue
            obj.SFDC_cip_tower = this.cip_tower == "" ? null : this.cip_tower;   // Shreyas 17-08-2020 FlatNo_issue
            obj.application_booking_18 = this.application_booking_18 == "" ? null : this.application_booking_18;
            obj.booking_status = this.booking_status == "" ? null : this.booking_status;
            obj.PropStrength__Booking_Cancelled_Date__c = this.PropStrength__Booking_Cancelled_Date__c == "" ? null : this.PropStrength__Booking_Cancelled_Date__c;
            obj.B1P__c = this.B1P__c == "" ? null : this.B1P__c;
            obj.B2P__c = this.B2P__c == "" ? null : this.B2P__c;
            obj.B3P__c = this.B3P__c == "" ? null : this.B3P__c;
            obj.B4P__c = this.B4P__c == "" ? null : this.B4P__c;
            obj.B5P__c = this.B5P__c == "" ? null : this.B5P__c;
            obj.B6P__c = this.B6P__c == "" ? null : this.B6P__c;
            obj.B7P__c = this.B7P__c == "" ? null : this.B7P__c;
            obj.TBP__c = this.TBP__c == "" ? null : this.TBP__c;
            obj.PropStrength__Brokerage_Amount__c = this.PropStrength__Brokerage_Amount__c == "" ? null : this.PropStrength__Brokerage_Amount__c;
            obj.propstrength__total_agreement_amount__c = this.propstrength__total_agreement_amount__c == "" ? null : this.propstrength__total_agreement_amount__c;
            return obj;
        }

    }
}
