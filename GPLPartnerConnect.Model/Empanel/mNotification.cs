﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mNotification
    {
        public string NotificationID {get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationBody { get; set; }
        public string ChannelType { get; set; }
        public string NotificationSourceID { get; set; }
        public string NotificationType { get; set; }
        public string MobileUrl { get; set; }
        public string ChannelID { get; set; }
        public string IsRedirect { get; set; }
        public string InsertedBy { get; set; }
        public string Active { get; set; }
        public string HasSend { get; set; }
        public string SendOn { get; set; }
        public string SubTitle { get; set; }
        public string ScheduledFor { get; set; }
        public string NotificationImage { get; set; }

        public string State { get; set; }
        public string City { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }


    }
}
