﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Empanel
{
    public class mOTP
    {
        public string OTPID { get; set; }
        public string pin { get; set; }
        public string mobileNumber { get; set; }
        public string EmplContID { get; set; }
        public string EmpanelmentID { get; set; }
        public string expireTime { get; set; }
        public string actionCode { get; set; }
        public string response { get; set; }
        public string SMSmessage { get; set; }
    }
}
