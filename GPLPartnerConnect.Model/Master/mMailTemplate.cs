﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.Master
{
    public class mMailTemplate
    {
        public string MailTemplateID { get; set; }
        public string MailTempleteCode { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Active { get; set; }
        public string InsertedBy { get; set; }
    }
}
