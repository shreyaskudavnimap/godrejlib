﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.FCM
{
    public class mMsg
    {
        public string to { get; set; }
        //public mNotification notification { get; set; }
        public mMsgData data { get; set; }

        public mMsg()
        {
            //notification = new mNotification();
            data = new mMsgData();
        }
    }
}
