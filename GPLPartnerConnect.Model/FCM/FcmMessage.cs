﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.FCM
{
    public class FcmMessage
    {
        public string to { get; set; }

        public data data;

        public notification notification;
        public FcmMessage()
        {
            data = new data();
            notification = new notification();
        }

    }

    public class data
    {
        public string sound = "default";
        public string body { get; set; }
        public string title { get; set; }

        public bool content_available = true;

        public string priority = "high";
        public string notificationLogID { get; set; }
        public string image { get; set; }
        public string subtitle { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }
    }

    public class notification
    {
        public string sound = "default";
        public string body { get; set; }
        public string title { get; set; }

        public bool content_available = true;

        public string priority = "high";
        public string notificationLogID { get; set; }
        public string image { get; set; }
        public string subtitle { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }
    }
}
