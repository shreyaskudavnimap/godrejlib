﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.FCM
{
    public class mResult
    {
        public string message_id { set; get; }
        public string error { set; get; }
    }
}
