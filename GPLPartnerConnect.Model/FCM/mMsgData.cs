﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.FCM
{
    public class mMsgData
    {
        public string ChannelType { get; set; }
        public string id { get; set; }
        public string NotificationLogID { get; set; }
        public string body { get; set; }
        public string title { get; set; }
        public string NotificationImage { get; set; }
    }
}
