﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.FCM
{
    public class mNotification
    {
        public string body { get; set; }
        public string title { get; set; }
    }
}
