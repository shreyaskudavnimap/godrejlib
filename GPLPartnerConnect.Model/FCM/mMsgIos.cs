﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.FCM
{
    public class mMsgIos
    {
        public string to { get; set; }
        public mMsgData notification { get; set; }          

        public mMsgIos()
        {
            notification = new mMsgData();              
        }
    }
}
