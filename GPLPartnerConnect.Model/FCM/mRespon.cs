﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Model.FCM
{
    public class mRespon
    {
        public string multicast_id { get; set; }
        public string success { get; set; }
        public string failure { get; set; }
        public string canonical_ids { get; set; }
        public List<mResult> results { get; set; }
        public mRespon()
        {
            results = new List<mResult>();
        }
    }
}
