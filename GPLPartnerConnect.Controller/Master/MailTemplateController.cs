﻿using GPLPartnerConnect.DAL.Master;
using GPLPartnerConnect.Model.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Controller.Master
{
    public class MailTemplateController
    {
        MailTemplateDAL objDAL = new MailTemplateDAL();

        public DataSet GetMailTempleteAll()
        {
            mMailTemplate mobj = new mMailTemplate();
            return objDAL.Call_SP_MailTemplate(mobj, "A");
        }

        public DataSet GetMailTempleteByID(string ID)
        {
            mMailTemplate mobj = new mMailTemplate();
            mobj.MailTemplateID = ID;
            return objDAL.Call_SP_MailTemplate(mobj, "B");
        }
        public mMailTemplate GetMailTempleteByCode(string Code)
        {
            mMailTemplate mobj = new mMailTemplate();
            mobj.MailTempleteCode = Code;
            DataSet ds = objDAL.Call_SP_MailTemplate(mobj, "E");
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                mobj.MailTemplateID = row["MailTemplateID"].ToString();
                mobj.MailTempleteCode = row["MailTempleteCode"].ToString();
                mobj.Subject = row["Subject"].ToString();
                mobj.Body = row["Body"].ToString();
                mobj.Active = row["Active"].ToString();

                return mobj;
            }

            return null;
        }

        public DataSet AddUpdateMailTemplete(mMailTemplate model, string strflag)
        {
            return objDAL.Call_SP_MailTemplate(model, strflag);
        }

    }
}
