﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Master;
using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Master;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class OTPController
    {
        OTPDAL objDAL = new OTPDAL();
        //string MessageText1 = ConfigurationManager.AppSettings["OTPMessage1"].ToString();
        //string MessageText2 = ConfigurationManager.AppSettings["OTPMessage2"].ToString();
        string MessageText1 = "Your verification code is ";
        string MessageText2 = " to access the EOI, Walk-in and Show booking detail section.";
        public int SendOTP(mEmpanelmentContact emp, string OTPAction)
        {
            string receiptMobNo = "91" + emp.Mobile;
            //Recepient = "91" + emp.Mobile;
            int OTP = GenerateOtp();

            string Msg = string.Concat(new string[]
            {
                this.MessageText1,
                " ",
                OTP.ToString(),
                " ",
                this.MessageText2
            });
            string resultmsg = SendSMS(OTP, receiptMobNo, Msg);

            //SMS Logging for perticular Option
            mOTP obj = new mOTP();
            obj.SMSmessage = Msg;
            obj.mobileNumber = receiptMobNo;
            obj.actionCode = OTPAction;
            obj.response = resultmsg;
            obj.EmplContID = emp.EmplContID;
            obj.EmpanelmentID = emp.EmpanelmentID;
            obj.pin = OTP.ToString();
            objDAL.Call_SP_OTP(obj, "A");
            return OTP;
        }

        public string SendSMS(int OTP, string receiptMobNo, string Msg)
        {
            var smsurl = "";
            string buzzifykey = ConfigurationManager.AppSettings["BUZZIFY_KEY"];        // Shreyas 19.08.2020 New_SmsApi
            smsurl = "https://buzzify.in/V2/http-api.php?apikey" + buzzifykey + "&senderid=godrej&number="+ receiptMobNo + "&message=" + Msg + "&format=json";
            //smsurl = "http://www.myvaluefirst.com/smpp/sendsms?username=godrejhtptrn&password=godrj001&to=" + receiptMobNo + "&from=GPLCHP&text=" + Msg;
            

            WebRequest request = WebRequest.Create(smsurl);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //read the response
            StreamReader responseReader = new StreamReader(response.GetResponseStream());
            return responseReader.ReadToEnd();
        }

        public void SendEmail(string OTP, mEmpanelmentContact emp)
        {
            MailTemplateController mailController = new MailTemplateController();
            mMailTemplate mailTe = mailController.GetMailTempleteByCode("OTP");
            if(mailTe != null)
            {
                EmailService emailService = new EmailService();
                emailService.properties.AddToEmail(emp.Email, emp.Name);
                string body = mailTe.Body;
                body = body.Replace("#EmpanelmentName#", emp.Name);
                body = body.Replace("#VERCODE#", OTP);
                emailService.properties.MailBody = body;
                emailService.properties.MailSubject = mailTe.Subject;
                emailService.SendMail();
            }

        }

        public mOTP ExistingOtp(string EmplContID)
        {
            mOTP obj = new mOTP();
            obj.EmplContID = EmplContID;
            DataSet ds = objDAL.Call_SP_OTP(obj, "B");
            if(ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                obj.pin = row["pin"].ToString();
                obj.mobileNumber = row["mobileNumber"].ToString();
                obj.SMSmessage = row["SMSmessage"].ToString();
                return obj;
            }
            return null;
        }

        public bool verifyOTP(string EmplContID, string pin)
        {
            mOTP obj = new mOTP();
            obj.EmplContID = EmplContID;
            obj.pin = pin;
            DataSet ds = objDAL.Call_SP_OTP(obj, "C");
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                if(row["Msg"].ToString() == "Y")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public int GenerateOtp()
        {
            Random r = new Random();
            int otpInt = r.Next(111111, 999999);
            return otpInt;
        }

    }
}
