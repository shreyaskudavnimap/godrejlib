﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Master;
using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Master;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class EmpanelmentController
    {
        EmpanelmentDAL obj = new EmpanelmentDAL();
        public mEmpanelment GetCPProperties(string emplId)
        {
            mEmpanelment empObj = new mEmpanelment();
            empObj.EmplID = emplId;
            DataSet ds = obj.EmpanelmentData(empObj, "C");
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                empObj.Communication_Street = row["Communication_Street"].ToString();
                empObj.Communication_City = row["Communication_City"].ToString();
                empObj.Communication_State = row["Communication_State"].ToString();
                empObj.Communication_Zip = row["Communication_Zip"].ToString();
                empObj.Communication_Country = row["Communication_Country"].ToString();
                empObj.Registered_City = row["Registered_City"].ToString();
                empObj.Registered_Country = row["Registered_Country"].ToString();
                empObj.Registered_State = row["Registered_State"].ToString();
                empObj.Registered_Street = row["Registered_Street"].ToString();
                empObj.Registered_Zip = row["Registered_Zip"].ToString();
                empObj.Billing_City = row["Billing_City"].ToString();
                empObj.Billing_Country = row["Billing_Country"].ToString();
                empObj.Billing_State = row["Billing_State"].ToString();
                empObj.Billing_Street = row["Billing_Street"].ToString();
                empObj.Billing_Zip = row["Billing_Zip"].ToString();
            }
            return empObj;
        }

        public DataSet GetEmpanelmentByPanNo(string panno)
        {
            mEmpanelment empl = new mEmpanelment();
            empl.Pan_No = panno;
            return obj.EmpanelmentData(empl, "D");
        }
        public DataSet GetEmpanelmentByEmail(string Email, string EmplID)
        {
            mEmpanelment empl = new mEmpanelment();
            empl.Email = Email;
            empl.EmplID = EmplID;
            return obj.EmpanelmentData(empl, "E");
        }
        public DataSet GetAllApproved()
        {
            mEmpanelment empl = new mEmpanelment();
            return obj.EmpanelmentData(empl, "F");
        }

        public mCommonOutput InsertEmpanelment(mEmpanelment empl)
        {
            mCommonOutput o = new mCommonOutput();
            if (empl.ReraDoc.Count > 0)
            {
                string[] reraState = new string[empl.ReraDoc.Count];
                string[] reraid = new string[empl.ReraDoc.Count];
                foreach (mReraDocs rera in empl.ReraDoc)
                {
                    if (reraState.Contains(rera.state))
                    {
                        o.status = "fail";
                        o.msg = rera.state + " state can only have one RERA";
                        return o;
                    }
                    if (reraid.Contains(rera.rerano))
                    {
                        o.status = "fail";
                        o.msg = "Duplicate RERA found";
                        return o;
                    }
                }

                string[] gstState = new string[empl.GstDoc.Count];
                string[] gstid = new string[empl.GstDoc.Count];
                foreach (mGstDocs gst in empl.GstDoc)
                {
                    if (gstState.Contains(gst.state))
                    {
                        o.status = "fail";
                        o.msg = gst.state + " state can only have one GST";
                        return o;
                    }
                    if (gstid.Contains(gst.gstno))
                    {
                        o.status = "fail";
                        o.msg = "Duplicate GST found";
                        return o;
                    }
                }

                DataSet ds;
                if (empl.EmplID == "0" || empl.EmplID == "" || empl.EmplID == null)
                {
                    empl.status = "P";
                    ds = obj.EmpanelmentData(empl, "A");
                }
                else
                {
                    empl.status = "P";
                    ds = obj.EmpanelmentData(empl, "B");
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    if (row["Status"].ToString() == "success")
                    {
                        string EmplID = ds.Tables[0].Rows[0]["EmplID"].ToString();
                        o.id = EmplID;
                        o.status = "success";

                        foreach (mReraDocs rera in empl.ReraDoc)
                        {
                            rera.emplid = EmplID;
                            if (rera.status == null)
                            {
                                InsertRera(rera);
                            }
                            else if (rera.status == "D")
                            {
                                DeleteRera(rera);
                            }
                        }
                        foreach (mGstDocs gst in empl.GstDoc)
                        {
                            gst.emplid = EmplID;
                            if (gst.status == null)
                            {
                                InsertGst(gst);
                            }
                            else if (gst.status == "D")
                            {
                                DeleteGst(gst);
                            }

                        }
                        try
                        {
                            EmailService es = new EmailService();
                            MailTemplateController mailCont = new MailTemplateController();
                            mMailTemplate mail = mailCont.GetMailTempleteByCode("THANK");
                            string email = empl.Email;
                            string name = empl.FirstName + ' ' + empl.LastName;
                            es.properties.AddToEmail(email, name);

                            es.properties.MailSubject = mail.Subject;
                            string Body = mail.Body;
                            Body = Body.Replace("#EmpanelmentName#", name);
                            es.properties.MailBody = Body;
                            es.SendMail();

                            es = new EmailService();
                            mail = mailCont.GetMailTempleteByCode("ADMIN");
                            es.properties.MailSubject = mail.Subject;
                            Body = mail.Body;

                            email = ConfigurationManager.AppSettings["AdminEmail"];
                            es.properties.AddToEmail(email);

                            string rera_state = "";
                            string rera_no = "";
                            string gst_state = "";
                            string gst_no = "";
                            string strPath = HttpContext.Current.Server.MapPath("~") + "/";
                            string Uplodefile = strPath + ConfigurationManager.AppSettings["AttachmentPhysicalPath"];
                            foreach (mReraDocs rera in empl.ReraDoc)
                            {
                                rera_state += rera.state;
                                rera_no += rera.rerano;
                                es.properties.MailAttachPath.Add(Uplodefile + rera.reracerti);
                            }
                            foreach (mGstDocs gst in empl.GstDoc)
                            {
                                gst_state += gst.state;
                                gst_no += gst.gstno;
                                es.properties.MailAttachPath.Add(Uplodefile + gst.gstcerti);
                            }
                            es.properties.MailAttachPath.Add(Uplodefile + empl.pancerti);

                            string commaddress = empl.Communication_Street + ' ' + empl.Communication_City + ' ' + empl.Communication_Zip + ' ' + empl.Communication_State + ' ' + empl.Communication_Country;
                            string regaddress = empl.Registered_Street + ' ' + empl.Registered_City + ' ' + empl.Registered_Zip + ' ' + empl.Registered_State + ' ' + empl.Registered_Country;



                            Body = Body.Replace("#user_name#", empl.FirstName + ' ' + empl.LastName).Replace("#user_mobile#", empl.Mobile).Replace("#user_email#", empl.Email)
                                .Replace("#user_company#", empl.Company_Name).Replace("#user_entitytype#", empl.entity_type).Replace("#user_commaddress#", commaddress)
                                .Replace("#user_regaddress#", regaddress).Replace("#user_pan#", empl.Pan_No).Replace("#user_rera_state#", rera_state).Replace("#user_rera_no#", rera_no)
                                .Replace("#user_gst_state#", gst_state).Replace("#user_gst_no#", gst_no);

                            es.properties.MailBody = Body;
                            es.SendMail();
                        }
                        catch (Exception ex)
                        {
                            WLog.WriteLog(ex.ToString(), "InsertEmpanelmentMailError");
                        }



                    }
                    else
                    {
                        o.status = "fail";
                        o.msg = row["Status"].ToString();
                    }


                    /*
                    MailMessage mail1 = new MailMessage();
                    SmtpClient SmtpServer1 = new SmtpClient(ConfigurationManager.AppSettings["ClientHost"]);

                    mail1.From = new MailAddress(ConfigurationManager.AppSettings["ClientFrom"]);
                    mail1.To.Add(empl.Email);
                    mail1.Subject = "Godrej Partner Connect APP | Empanelment request";
                    mail1.IsBodyHtml = true;
                    string path1 = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    StreamReader sr1 = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/empl_user.html"));
                    mail1.Body = sr1.ReadToEnd().Replace("user_name", empl.FirstName + ' ' + empl.LastName);
                    SmtpServer1.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                    SmtpServer1.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ClientFrom"], ConfigurationManager.AppSettings["ClientPwd"]);
                    SmtpServer1.EnableSsl = true;
                    SmtpServer1.Send(mail1);
                    sr1.Close();

                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["ClientHost"]);
                    mail.From = new MailAddress(ConfigurationManager.AppSettings["ClientFrom"]);

                    mail.To.Add("sujata.mirge@atritechnocrat.com");
                    //mail.To.Add("swati.saxena@godrejproperties.com");
                    mail.To.Add("snehal.lanjekar@godrejproperties.com");

                    mail.Subject = "CP Documents";
                    mail.IsBodyHtml = true;
                    string Uplodefile = ConfigurationManager.AppSettings["AttachmentPhysicalPath"];
                    mail.Attachments.Add(new Attachment(Uplodefile + empl.pancerti));
                    string rera_state = "";
                    string rera_no = "";
                    string gst_state = "";
                    string gst_no = "";
                    if (dtrera.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtrera.Rows)
                        {
                            rera_state += row["state"].ToString();
                            rera_no += row["rerano"].ToString();
                            mail.Attachments.Add(new Attachment(Uplodefile + row["reracerti"].ToString()));
                        }
                    }

                    if (dtgst.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtgst.Rows)
                        {
                            gst_state += row["state"].ToString();
                            gst_no += row["gstno"].ToString();
                            mail.Attachments.Add(new Attachment(Uplodefile + row["gstcerti"].ToString()));
                        }
                    }
                    string commaddress = empl.Communication_Street + ' ' + empl.Communication_City + ' ' + empl.Communication_Zip + ' ' + empl.Communication_State + ' ' + empl.Communication_Country;
                    string regaddress = empl.Registered_Street + ' ' + empl.Registered_City + ' ' + empl.Registered_Zip + ' ' + empl.Registered_State + ' ' + empl.Registered_Country;

                    string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Mailer/admin_mail.html"));
                    mail.Body = sr.ReadToEnd().Replace("user_name", empl.FirstName + ' ' + empl.LastName).Replace("user_mobile", empl.Mobile).Replace("user_email", empl.Email).Replace("user_company", empl.Company_Name).Replace("user_entitytype", empl.entity_type).Replace("user_commaddress", commaddress).Replace("user_regaddress", regaddress).Replace("user_pan", empl.Pan_No)
                    .Replace("user_rera_state", rera_state).Replace("user_rera_no", rera_no).Replace("user_gst_state", gst_state).Replace("user_gst_no", gst_no);
                    SmtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                    // SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    // SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ClientFrom"], ConfigurationManager.AppSettings["ClientPwd"]);
                    SmtpServer.EnableSsl = true;
                    SmtpServer.Send(mail);
                    sr.Close();
                    */
                }
                else
                {
                    o.status = "fail";
                    o.msg = "";
                }
            }
            else
            {
                o.status = "fail";
                o.msg = "RERA details missing";
            }

            return o;
        }

        public void InsertRera(mReraDocs rera)
        {
            obj.EmplReraDocsData(rera, "A");
        }

        public void InsertGst(mGstDocs gst)
        {
            obj.EmplGstDocsData(gst, "A");
        }
        public void DeleteRera(mReraDocs rera)
        {
            obj.EmplReraDocsData(rera, "B");
        }

        public void DeleteGst(mGstDocs gst)
        {
            obj.EmplGstDocsData(gst, "B");
        }
        public void EmplLogout(mEmplLogout logout)
        {
            obj.EmplLogout(logout, "A");
        }
        public mEmplLoginDetails GetEmplLoginDetails(mEmplLogin login)
        {
            DataSet ds = obj.GetEmplLoginDetails(login);
            if (ds.Tables[0].Rows.Count > 0)
            {
                mEmplLoginDetails mobj = new mEmplLoginDetails();
                DataRow row = ds.Tables[0].Rows[0];
                mobj.EmplContID = row["EmplContID"].ToString();
                mobj.EmpanelmentID = row["EmpanelmentID"].ToString();
                mobj.BrokerID = row["BrokerID"].ToString();
                mobj.Name = row["Name"].ToString();
                mobj.BrokerAccountID = row["BrokerAccountID"].ToString();
                mobj.Email = row["email"].ToString();
                mobj.PanNo = row["pan_no"].ToString();
                mobj.Mobile = row["Mobile"].ToString();
                mobj.BrokerContactID = row["BrokerContactID"].ToString();
                mobj.Status = row["Status"].ToString();
                mobj.CompanyName = row["Company_Name"].ToString();
                mobj.isSentOTP = row["isSentOTP"].ToString();
                mobj.region = row["region"].ToString();
                return mobj;
            }
            return null;
        }




        public DataSet GetEmpanelmentContactDetailsByEmail(string email)
        {
            mEmpanelmentContact mobj = new mEmpanelmentContact();
            mobj.Email = email;
            return obj.EmpanelmentContactData(mobj, "C");
        }
        public DataSet GetEmpanelmentContactDetailsByID(string EmplContID)
        {
            mEmpanelmentContact mobj = new mEmpanelmentContact();
            mobj.EmplContID = EmplContID;
            return obj.EmpanelmentContactData(mobj, "A");
        }
        public void UpdatePassword(string EmplContID, string password)
        {
            mEmpanelmentContact mobj = new mEmpanelmentContact();
            mobj.EmplContID = EmplContID;
            mobj.password = password;
            obj.EmpanelmentContactData(mobj, "B");
        }

        public mEmpanelmentContact GetEmpanelmentContactDetails(string EmplContID)
        {
            mEmpanelmentContact mobj = new mEmpanelmentContact();
            mobj.EmplContID = EmplContID;
            DataSet ds = obj.EmpanelmentContactData(mobj, "A");

            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                mobj.EmplContID = row["EmplContID"].ToString();
                mobj.Name = row["Name"].ToString();
                mobj.Mobile = row["Mobile"].ToString();
                mobj.Email = row["Email"].ToString();
                mobj.Pan_No = row["Pan_No"].ToString();
                mobj.Service_Tax_No = row["Service_Tax_No"].ToString();
                mobj.TIN_No = row["TIN_No"].ToString();
                mobj.Company_Name = row["Company_Name"].ToString();
                mobj.Enterprise_Membership_ID = row["Enterprise_Membership_ID"].ToString();
                mobj.Region = row["Region"].ToString();
                mobj.BrokerID = row["BrokerID"].ToString();
                mobj.SAP_Vendor_Code = row["SAP_Vendor_Code"].ToString();
                mobj.username = row["username"].ToString();
                mobj.password = row["password"].ToString();
                mobj.createddate = row["createddate"].ToString();
                mobj.Request_Pwd = row["Request_Pwd"].ToString();
                mobj.Deviceid = row["Deviceid"].ToString();
                mobj.platform = row["platform"].ToString();
                mobj.mailsend = row["mailsend"].ToString();
                mobj.pwdsend = row["pwdsend"].ToString();
                mobj.ProfilePic = row["ProfilePic"].ToString();
                mobj.BrokerAccountID = row["BrokerAccountID"].ToString();
                mobj.BrokerContactID = row["BrokerContactID"].ToString();
                mobj.Status = row["Status"].ToString();
                mobj.PanCerti = row["PanCerti"].ToString();
                mobj.EmpanelmentID = row["EmpanelmentID"].ToString();
                mobj.Billing_City = row["Billing_City"].ToString();
                mobj.Billing_Zip = row["Billing_Zip"].ToString();
                mobj.Billing_Street = row["Billing_Street"].ToString();
                mobj.Billing_State = row["Billing_State"].ToString();
                mobj.Billing_Country = row["Billing_Country"].ToString();
                mobj.Registered_City = row["Registered_City"].ToString();
                mobj.Registered_Zip = row["Registered_Zip"].ToString();
                mobj.Registered_Street = row["Registered_Street"].ToString();
                mobj.Registered_State = row["Registered_State"].ToString();
                mobj.Registered_Country = row["Registered_Country"].ToString();
                mobj.Communication_City = row["Communication_City"].ToString();
                mobj.Communication_Zip = row["Communication_Zip"].ToString();
                mobj.Communication_Street = row["Communication_Street"].ToString();
                mobj.Communication_State = row["Communication_State"].ToString();
                mobj.Communication_Country = row["Communication_Country"].ToString();
                return mobj;
            }
            return null;
        }
        public string GenerateRandomString(int length)
        {
            //It will generate string with combination of small,capital letters and numbers
            char[] charArr = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".ToCharArray();
            string randomString = string.Empty;
            Random objRandom = new Random();
            for (int i = 0; i < length; i++)
            {
                //Don't Allow Repetation of Characters
                int x = objRandom.Next(1, charArr.Length);
                if (!randomString.Contains(charArr.GetValue(x).ToString()))
                    randomString += charArr.GetValue(x);
                else
                    i--;
            }
            return randomString;


        }

        public DataSet InsertUpdateEmplContDeviceLog(mEmplContDeviceLog mobj)
        {
            return obj.EmplContDeviceLogData(mobj, "A");
        }

        public DataSet UpdateLastAccessed(mEmplContDeviceLog mobj)
        {
            return obj.EmplContDeviceLogData(mobj, "B");
        }

        public DataSet SetActiveNo(mEmplContDeviceLog mobj)
        {
            return obj.EmplContDeviceLogData(mobj, "C");
        }
        public DataSet GetActiveDevice()
        {
            mEmplContDeviceLog mobj = new mEmplContDeviceLog();
            return obj.EmplContDeviceLogData(mobj, "D");
        }

        public DataSet GetActiveDevice2(string state, string city)
        {
            mEmplContDeviceLog mobj = new mEmplContDeviceLog();
            mobj.State = state;
            mobj.City = city;
            return obj.EmplContDeviceLogData(mobj, "PN");
        }
    }
}
