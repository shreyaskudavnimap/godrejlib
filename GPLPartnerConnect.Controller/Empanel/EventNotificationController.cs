﻿using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GPLPartnerConnect.Controller.Empanel
{
    public class EventNotificationController
    {
        EventNotificationDAL objDAL = new EventNotificationDAL();

        public DataSet InsertUpdateNotification(mEventNotification obj)
        {
            return objDAL.EventNotificationData(obj, "A");
        }

        public DataSet GetNotificationAll(string EventID)
        {
            mEventNotification obj = new mEventNotification();
            obj.EventID = EventID;
            return objDAL.EventNotificationData(obj, "B");
        }
        public DataSet GetNotificationByID(string EventNotificationID)
        {
            mEventNotification obj = new mEventNotification();
            obj.EventNotificationID = EventNotificationID;
            return objDAL.EventNotificationData(obj, "C");
        }

        public DataSet UpdateNotificationSendStatus(string EventNotificationID)
        {
            mEventNotification obj = new mEventNotification();
            obj.EventNotificationID = EventNotificationID;
            return objDAL.EventNotificationData(obj,"D");
        }
    }
}
