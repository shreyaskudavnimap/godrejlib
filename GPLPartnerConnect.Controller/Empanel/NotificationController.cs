﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class NotificationController
    {
        NotificationDAL objDAL = new NotificationDAL();
           
        public DataSet GetRegionsPushNotifications(string flag, string selected_state)
        {
            mNotification obj = new mNotification();
            obj.State = selected_state;
            return objDAL.NotificationData(obj, flag);
        }

        public DataSet InsertUpdateNotification(mNotification obj)
        {
            return objDAL.NotificationData(obj, "A");
        }
        public DataSet GetNotificationAll()
        {
            mNotification obj = new mNotification();
            return objDAL.NotificationData(obj, "B");
        }
        public DataSet GetNotificationByID(string NotificationID)
        {
            mNotification obj = new mNotification();
            obj.NotificationID = NotificationID;
            return objDAL.NotificationData(obj, "C");
        }
        public DataSet UpdateNotificationSendStatus(string NotificationID)
        {
            mNotification obj = new mNotification();
            obj.NotificationID = NotificationID;
            return objDAL.NotificationData(obj, "D");
        }  
        public int InsertNotificationLog(mNotificationLog obj)
        {
            return Convert.ToInt32( objDAL.NotificationLogData(obj, "A").Tables[0].Rows[0]["NotificationLogID"]);
        }
        public DataSet UpdateNotificationLog(mNotificationLog obj)
        {
            return objDAL.NotificationLogData(obj, "B");
        }
        public DataSet GetNotificationLog(string EmplContDeviceLogID)
        {
            mNotificationLog obj = new mNotificationLog();
            obj.EmplContDeviceLogID = EmplContDeviceLogID;
            return objDAL.NotificationLogData(obj, "C");
        }
        public DataSet GetNotificationReady()
        {
            mNotification obj = new mNotification();
            return objDAL.NotificationData(obj, "E");
        }
        public string SendNotification(DataTable dt, string userid)
        {
            EmpanelmentController objEmpCon = new EmpanelmentController();
          
            int cnt = 0, totaldevicecnt = 0;
            mNotificationLog obj = new mNotificationLog();
            foreach (DataRow row in dt.Rows)
            {
                obj.NotificationID = row["NotificationID"].ToString();
                obj.NotificationTitle = row["NotificationTitle"].ToString();
                obj.NotificationBody = row["NotificationBody"].ToString();
                obj.NotificationSourceID = row["NotificationSourceID"].ToString();
                obj.NotificationType = row["NotificationType"].ToString();
                obj.ChannelType = row["ChannelType"].ToString();
                obj.MobileUrl = row["MobileUrl"].ToString();
                obj.ChannelID = row["ChannelID"].ToString();
                //obj.SendStatus = row["SendStatus"].ToString();
                //obj.ErrorMsg = row["ErrorMsg"].ToString();
                obj.IsRedirect = row["IsRedirect"].ToString();
                obj.InsertedBy = userid;
                obj.Active = "Y";
                obj.SubTitle = row["SubTitle"].ToString();
                obj.NotificationImage = row["NotificationImage"].ToString();
                obj.Link = row["Link"].ToString();
                obj.LinkText = row["LinkText"].ToString();

                string state = row["State"].ToString();
                string city = row["City"].ToString();
                DataSet dsEmp = objEmpCon.GetActiveDevice2(state, city);
                totaldevicecnt += dsEmp.Tables[0].Rows.Count;
                foreach (DataRow rowEmp in dsEmp.Tables[0].Rows)
                {
                    obj.SendStatus = "";
                    obj.ErrorMsg = "";
                    obj.EmplContDeviceLogID = rowEmp["EmplContDeviceLogID"].ToString();
                    obj.EmplContID = rowEmp["EmplContID"].ToString();
                    obj.DeviceToken = rowEmp["DeviceToken"].ToString();
                    obj.MobilePlatform = rowEmp["MobilePlatform"].ToString();   

                    obj.NotificationLogID = InsertNotificationLog(obj).ToString();


                    Model.FCM.mRespon fcmResp = null;

                    Model.FCM.FcmMessage fcmmsg = new Model.FCM.FcmMessage();
                    fcmmsg.to = obj.DeviceToken;

                    fcmmsg.data.title = obj.NotificationTitle;
                    fcmmsg.data.body = obj.NotificationBody;
                    fcmmsg.data.subtitle = obj.SubTitle;
                    fcmmsg.data.notificationLogID = obj.NotificationID;
                    fcmmsg.data.image = obj.NotificationImage;
                    fcmmsg.data.Link = obj.Link;
                    fcmmsg.data.LinkText = obj.LinkText;

                    fcmmsg.notification.title = obj.NotificationTitle;
                    fcmmsg.notification.body = obj.NotificationBody;
                    fcmmsg.notification.subtitle = obj.SubTitle;
                    fcmmsg.notification.notificationLogID = obj.NotificationID;
                    fcmmsg.notification.image = obj.NotificationImage;
                    fcmmsg.notification.Link = obj.Link;
                    fcmmsg.notification.LinkText = obj.LinkText;

                    fcmResp = GPLNotification.Instance.SendNotification(fcmmsg);

                    //if (obj.MobilePlatform == "Ios")                // Shreyas 14-07-2020
                    //{
                    //    Model.FCM.mMsgIos fcmmsg = new Model.FCM.mMsgIos();
                    //    fcmmsg.to = obj.DeviceToken;
                    //    fcmmsg.notification.title = obj.NotificationTitle;
                    //    fcmmsg.notification.body = obj.SubTitle;
                    //    fcmmsg.notification.ChannelType = obj.ChannelType;
                    //    fcmmsg.notification.id = obj.ChannelID;
                    //    fcmmsg.notification.NotificationLogID = obj.NotificationLogID;
                    //    fcmResp = GPLNotification.Instance.SendNotificationIos(fcmmsg);
                    //}
                    //else
                    //{
                    //    Model.FCM.mMsg fcmmsg = new Model.FCM.mMsg();
                    //    fcmmsg.to = obj.DeviceToken;
                    //    fcmmsg.data.title = obj.NotificationTitle;
                    //    fcmmsg.data.body = obj.SubTitle;
                    //    fcmmsg.data.ChannelType = obj.ChannelType;
                    //    fcmmsg.data.id = obj.ChannelID;
                    //    fcmmsg.data.NotificationLogID = obj.NotificationID;
                    //    fcmResp = GPLNotification.Instance.SendNotification(fcmmsg);
                    //}
                    
                    cnt += Convert.ToInt16(fcmResp.success);
                    if (fcmResp.success == "0")
                    {
                        obj.SendStatus = "N";
                        obj.ErrorMsg = fcmResp.results[0].error;
                    }
                    else
                    {
                        obj.SendStatus = "Y";
                    }
                    //WLog.WriteLog("httpResponse = " + responseText, "PushNotification");

                    UpdateNotificationLog(obj);
                }
                UpdateNotificationSendStatus(obj.NotificationID);
                if(obj.ChannelType == "offer")
                {
                    new OfferNotificationController().UpdateNotificationSendStatus(obj.NotificationSourceID);
                }
                else if (obj.ChannelType  == "event")
                {
                    new EventNotificationController().UpdateNotificationSendStatus(obj.NotificationSourceID);
                }
                else if (obj.ChannelType == "project")
                {
                    new ProjectNotificationController().UpdateNotificationSendStatus(obj.NotificationSourceID);
                }
            }
            return cnt + " of " + totaldevicecnt;
        }
    }
}
