﻿using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class BrokerSFDCDataController
    {
        BrokerSFDCDataDAL objDAL = new BrokerSFDCDataDAL();
        public void DeleteBrokerBooking(string EmplID, string EmplContID)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.EmplID = EmplID;
            objDAL.BrokerBookingData(obj, "A");
        }

        public void DeleteBrokerEnquiry(string EmplID, string EmplContID)
        {
            mBrokerEnquiry obj = new mBrokerEnquiry();
            obj.EmplContID = EmplContID;
            obj.EmplID = EmplID;
            objDAL.BrokerEnquiryData(obj, "A");
        }

        public void AddBrokerBooking(mBrokerBooking obj)
        {
            objDAL.BrokerBookingData(obj, "B");
        }

        public void AddBrokerEnquiry(mBrokerEnquiry obj)
        {
            objDAL.BrokerEnquiryData(obj, "B");
        }

        public DataSet GetBrokerBookingQuarter(string EmplContID, string FiscalYearID)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.EmplID = FiscalYearID;
            return objDAL.BrokerBookingData(obj, "C");
        }

        public DataSet GetBrokerEnquiryQuarter(string EmplContID, string FiscalYearID)
        {
            mBrokerEnquiry obj = new mBrokerEnquiry();
            obj.EmplContID = EmplContID;
            obj.EmplID = FiscalYearID;
            return objDAL.BrokerEnquiryData(obj, "C");
        }
        //public DataSet GetBrokerBookingData(string EmplContID, string quarter, string projectName)
        public DataSet GetBrokerBookingData(string EmplContID, string quarter, string projectName)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.SFDC_PropStrengthBookingDateC = quarter;
            obj.title = projectName;
            //obj.SFDC_PropStrengthProjectC = SFDCProjectID;
            return objDAL.BrokerBookingData(obj, "D");
        }

        public DataSet GetBrokerBookingCancelData(string EmplContID, string quarter, string SFDCProjectID)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.SFDC_PropStrengthBookingDateC = quarter;
            obj.SFDC_PropStrengthProjectC = SFDCProjectID;
            return objDAL.BrokerBookingCancelData(obj, "LINE");
        }

        //public DataSet GetBrokerEnquiryData(string EmplContID, string quarter, string sfdcprojectname)
        public DataSet GetBrokerEnquiryData(string EmplContID, string quarter, string sfdcprojectname)
        {
            mBrokerEnquiry obj = new mBrokerEnquiry();
            obj.EmplContID = EmplContID;
            obj.SFDC_DateOfSiteVisitC = quarter;
            obj.title = sfdcprojectname;
            //obj.SFDC_PropStrengthProjectC = SFDCProjectID;
            return objDAL.BrokerEnquiryData(obj, "D");
        }
        public DataSet GetBrokerBookingSummary(string EmplContID, string quarter)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.SFDC_PropStrengthBookingDateC = quarter;
            return objDAL.BrokerBookingData(obj, "E");
        }
        public DataSet GetBrokerBookingCancelSummary(string EmplContID, string quarter)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.SFDC_PropStrengthBookingDateC = quarter;
            return objDAL.BrokerBookingCancelData(obj, "GET");
        }

        public DataSet GetBrokerEnquirySummary(string EmplContID, string quarter)
        {
            mBrokerEnquiry obj = new mBrokerEnquiry();
            obj.EmplContID = EmplContID;
            obj.SFDC_DateOfSiteVisitC = quarter;
            return objDAL.BrokerEnquiryData(obj, "E");
        }
        public DataSet GetBrokerBookingSummaryDetail(string EmplContID, string quarter, string Location)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.SFDC_PropStrengthBookingDateC = quarter;
            obj.SFDC_Region = Location;
            return objDAL.BrokerBookingData(obj, "F");
        }

        public DataSet GetBrokerBookingCancelSummaryDetail(string EmplContID, string quarter, string Location)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.SFDC_PropStrengthBookingDateC = quarter;
            obj.SFDC_Region = Location;
            return objDAL.BrokerBookingCancelData(obj, "DETAILS");
        }
        public DataSet GetBrokerEnquirySummaryDetail(string EmplContID, string quarter, string Location)
        {
            mBrokerEnquiry obj = new mBrokerEnquiry();
            obj.EmplContID = EmplContID;
            obj.SFDC_DateOfSiteVisitC = quarter;
            obj.SFDC_Region = Location;
            return objDAL.BrokerEnquiryData(obj, "F");
        }
        public DataSet GetBrokerBookingFiscalYear(string EmplContID)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            return objDAL.BrokerBookingData(obj, "G");
        }
        public DataSet GetBrokerEnquiryFiscalYear(string EmplContID)
        {
            mBrokerEnquiry obj = new mBrokerEnquiry();
            obj.EmplContID = EmplContID;
            return objDAL.BrokerEnquiryData(obj, "G");
        }
        public DataSet GetBrokerBookingExcel(string EmplContID, string FiscalYearID)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.EmplID = FiscalYearID;
            return objDAL.BrokerBookingData(obj, "H");
        }

        public DataSet GetBrokerBookingCancelExcel(string EmplContID, string FiscalYearID)
        {
            mBrokerBooking obj = new mBrokerBooking();
            obj.EmplContID = EmplContID;
            obj.EmplID = FiscalYearID;
            return objDAL.BrokerBookingCancelData(obj, "EXCEL");
        }
        public DataSet GetBrokerEnquiryExcel(string EmplContID, string FiscalYearID)
        {
            mBrokerEnquiry obj = new mBrokerEnquiry();
            obj.EmplContID = EmplContID;
            obj.EmplID = FiscalYearID;
            return objDAL.BrokerEnquiryData(obj, "H");
        }
    }
}