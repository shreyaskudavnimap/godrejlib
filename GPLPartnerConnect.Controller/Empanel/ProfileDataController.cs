﻿using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class ProfileDataController
    {
        ProfileDataDAL objDAL = new ProfileDataDAL();
        public DataSet GetProfileData(string EmplId)
        {
            mProfileData obj = new mProfileData();
            obj.EmplId = EmplId;
            return objDAL.GetProfileData(obj, "GET_DETAILS");
        }

        public DataSet GetUserProfileDetails(string EmplId)
        {
            mProfileData obj = new mProfileData();
            obj.EmplId = EmplId;
            return objDAL.GetProfileData(obj, "GET_ALL_DETAILS");
        }

        public string SendOTP(string emplContID, string newContact)
        {
            mContactOTP obj = new mContactOTP();
            obj.EmplId = emplContID;
            obj.OTPNo = GenerateRandomNo().ToString();
            objDAL.InsertOTP(obj, "OTP");

            mProfileData objProfile = new mProfileData();
            objProfile.EmplId = emplContID;
            string message = obj.OTPNo + " is your OTP for verification. OTP is confidential. For security reasons, DO NOT SHARE THIS OTP WITH ANYONE. Regards, Godrej Properties";
            return SendSms(newContact, message);
        }

        public string UpdateContact(string emplContID, string vertifyOtp, string newContact, string oldContact, string input_user, string input_pass)
        {
            DataSet ds = objDAL.UpdateContact(emplContID, vertifyOtp, newContact, oldContact, input_user, input_pass, "CNFRM_OTP");
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public DataSet GetUserDetailsForAlert(string emplContID)
        {
            return objDAL.GetUserDetailsForAlert(emplContID, "ALERT");
        }

        public string UpdateEmail(string emplContID, string newEmail, string input_user, string input_pass)
        {
            DataSet ds = objDAL.UpdateEmail(emplContID, newEmail, input_user, input_pass, "EMAIL");
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public void AddRera(mReraUpdate reraData)
        {
            string emplId = reraData.UserId;
            foreach(var rera in reraData.list)
            {
                if (rera.insertflag == true) {
                    objDAL.AddRera(emplId, rera.rerano, rera.state, rera.reracerti);
                }
            }
        }

        public DataSet GetEmplReraGst(string emplContID, string docType)
        {
            DataSet ds = objDAL.GetEmplReraGst(emplContID, docType);
            return ds;
        }

        public void AddGst(mGstUpdate gstData)
        {
            string emplId = gstData.UserId;
            foreach (var gst in gstData.list)
            {
                if (gst.insertflag == true)
                {
                    objDAL.AddGst(emplId, gst.gstno, gst.state, gst.gstcerti);
                }
            }
        }

        public string ValidateLogin(string emplContID, string input_user, string input_pass)
        {
            DataSet ds = objDAL.ValidateLogin(emplContID, input_user, input_pass, "LOGIN");
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public string UpdateCommAddress(string emplContID, string newCommAddress, string newCommCity, string newCommState, string newCommZipPostal, string input_user, string input_pass, string newCommCountry)
        {
            DataSet ds = objDAL.UpdateCommAddress(emplContID, newCommAddress, newCommCity, newCommState, newCommZipPostal, input_user, input_pass, "COMM_ADDR", newCommCountry);
            return ds.Tables[0].Rows[0][0].ToString();
        }
        public string UpdateRegAddress(string emplContID, string newRegAddress, string newRegCountry, string newRegState, string newRegCity, string newRegZipCode, string addrProof)
        {
            DataSet ds = objDAL.UpdateRegAddress(emplContID, newRegAddress, newRegCountry, newRegState, newRegCity, newRegZipCode, addrProof, "REG_ADDR");
            return ds.Tables[0].Rows[0][0].ToString();
        }

        public int GenerateRandomNo()
        {
            int _min = 100000;
            int _max = 999999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public string SendSms(string number, string message)
        {
            try
            {
                string buzzifykey = ConfigurationManager.AppSettings["BUZZIFY_KEY"].ToString();
                string smsurl = "https://buzzify.in/V2/http-api.php?apikey=" + buzzifykey + "&senderid=godrej&number= " + number + " &message=" + message + "&format=json";
                
                WebRequest request = WebRequest.Create(smsurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //read the response
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                return responseReader.ReadToEnd();
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public void SendMail(string FromMail, string ToMail, string user, string pass, string subject, string body)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(FromMail);
            msg.To.Add(ToMail);
            msg.Subject = subject;
            msg.Body = body;

            using (SmtpClient client = new SmtpClient())
            {
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(user, pass);
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.Send(msg);
            }
        }
    }
}
