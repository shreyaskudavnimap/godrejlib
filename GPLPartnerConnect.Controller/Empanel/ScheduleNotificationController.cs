﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class ScheduleNotificationController
    {
        ScheduleNotificationDAL objDAL = new ScheduleNotificationDAL();

        public DataSet GetRegionsPushNotifications(string flag, string selected_state)
        {
            mNotification obj = new mNotification();
            obj.State = selected_state;
            return objDAL.NotificationData(obj, flag);
        }

        public DataSet InsertUpdateScheduleNotification(mScheduleNotification obj)
        {
            return objDAL.ScheduleNotificationData(obj, "INSERT");
        }
        public DataSet GetScheduleNotificationAll()
        {
            mScheduleNotification obj = new mScheduleNotification();
            return objDAL.ScheduleNotificationData(obj, "ALL");
        }
        public DataSet GetScheduleNotificationByID(string NotificationID)
        {
            mScheduleNotification obj = new mScheduleNotification();
            obj.NotificationID = NotificationID;
            return objDAL.ScheduleNotificationData(obj, "ID");
        }

        public DataSet GetTemplateById(int templateId)
        {
            return objDAL.GetTemplateById(templateId);
        }

        public DataSet DeleteNotificationByID(string notificationId)
        {
            return objDAL.DeleteNotificationByID(notificationId, "DELETE");
        }

        
        //public DataSet UpdateNotificationSendStatus(string NotificationID)
        //{
        //    mScheduleNotification obj = new mScheduleNotification();
        //    obj.NotificationID = NotificationID;
        //    return objDAL.ScheduleNotificationData(obj, "D");
        //}  
        //public DataSet GetNotificationReady()
        //{
        //    mScheduleNotification obj = new mScheduleNotification();
        //    return objDAL.ScheduleNotificationData(obj, "E");
        //}

    }
}
