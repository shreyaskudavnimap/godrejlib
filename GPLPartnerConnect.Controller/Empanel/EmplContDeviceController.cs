﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class EmplContDeviceController
    {
        EmplContDeviceDAL objDAL = new EmplContDeviceDAL();
        public DataSet GetEmplContDeviceId(string deviceToken)
        {
            return objDAL.GetEmplContDeviceId(deviceToken);
        }
    }
}
