﻿using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class EOIController
    {
        EOIDAL objDAL = new EOIDAL();
        public string SendOTP(mEOI obj)
        {
            obj.OTPNo = GenerateRandomNo().ToString();
            objDAL.InsertOTP(obj, "OTP");
            string message = obj.OTPNo + " is your OTP for verification. OTP is confidential. For security reasons, DO NOT SHARE THIS OTP WITH ANYONE. Regards, Godrej Properties";
            return SendSms(obj.Mobile, message);
        }

        public int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public string SendSms(string number, string message)
        {
            try
            {
                string buzzifykey = ConfigurationManager.AppSettings["BUZZIFY_KEY"].ToString();
                string smsurl = "https://buzzify.in/V2/http-api.php?apikey=" + buzzifykey + "&senderid=godrej&number= " + number + " &message=" + message + "&format=json";

                WebRequest request = WebRequest.Create(smsurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //read the response
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                return responseReader.ReadToEnd();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ValidateOTP(mEOI obj)
        {
            return objDAL.ValidateOTP(obj, "VERIFY_OTP");
        }
    }
}
