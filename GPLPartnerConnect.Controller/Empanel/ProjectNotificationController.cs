﻿using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class ProjectNotificationController
    {
        ProjectNotificationDAL objDAL = new ProjectNotificationDAL();

        public DataSet InsertUpdateNotification(mProjectNotification obj)
        {
            return objDAL.ProjectNotificationData(obj, "A");
        }

        public DataSet GetNotificationAll(string ProjectID)
        {
            mProjectNotification obj = new mProjectNotification();
            obj.ProjectID = ProjectID;
            return objDAL.ProjectNotificationData(obj, "B");
        }
        public DataSet GetNotificationByID(string ProjectNotificationID)
        {
            mProjectNotification obj = new mProjectNotification();
            obj.ProjectNotificationID = ProjectNotificationID;
            return objDAL.ProjectNotificationData(obj, "C");
        }

        public DataSet UpdateNotificationSendStatus(string ProjectNotificationID)
        {
            mProjectNotification obj = new mProjectNotification();
            obj.ProjectNotificationID = ProjectNotificationID;
            return objDAL.ProjectNotificationData(obj, "D");
        }

    }
}