﻿using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class OfferNotificationController
    {
        OfferNotificationDAL objDAL = new OfferNotificationDAL();



        public DataSet InsertUpdateNotification(mOfferNotification obj)
        {
            return objDAL.OfferNotificationData(obj, "A");
        }

        public DataSet GetNotificationAll(string OfferID)
        {
            mOfferNotification obj = new mOfferNotification();
            obj.OfferID = OfferID;
            return objDAL.OfferNotificationData(obj, "B");
        }
        public DataSet GetNotificationByID(string OfferNotificationID)
        {
            mOfferNotification obj = new mOfferNotification();
            obj.OfferNotificationID = OfferNotificationID;
            return objDAL.OfferNotificationData(obj, "C");
        }

        public DataSet UpdateNotificationSendStatus(string OfferNotificationID)
        {
            mOfferNotification obj = new mOfferNotification();
            obj.OfferNotificationID = OfferNotificationID;
            return objDAL.OfferNotificationData(obj, "D");
        }
    }
}
