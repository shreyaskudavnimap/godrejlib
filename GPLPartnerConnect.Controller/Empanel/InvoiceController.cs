﻿using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class InvoiceController
    {
        InvoiceDAL objDAL = new InvoiceDAL();

        public DataSet GetRegionList(mInvoices obj)
        {
            return objDAL.GetInvoiceSummary(obj, "REG_LST");
        }

        public DataSet GetProjectList(mInvoices obj)
        {
            return objDAL.GetInvoiceSummary(obj, "PROJ_LST");
        }

        public DataSet GetBrokerInvoiceFiscalYear(mInvoices obj)
        {
            return objDAL.GetInvoiceSummary(obj, "GET_YR");
        }

        public DataSet GetBrokerInvoiceQuarter(string EmplContID, string FiscalYearID)
        {
            mInvoices obj = new mInvoices();
            obj.EmplId = EmplContID;
            obj.FiscalYearID = FiscalYearID;
            return objDAL.GetInvoiceSummary(obj, "GET_QRTR");
        }

        public DataSet GetInvoiceData(string emplId, string quarter, string projectid, string year)
        {
            mInvoiceSummaryDetail obj = new mInvoiceSummaryDetail();
            obj.EmplId = emplId;
            obj.Quarters = quarter;
            obj.ProjectId = projectid;
            obj.Year = year;
            return objDAL.InvoiceData(obj, "S2C");
        }

        public DataSet GetProjectInvoices(mInvoices obj)
        {
            return objDAL.GetInvoiceSummary(obj, "PROJ_INV");
        }

        public DataSet GetProfileData(string emplId)
        {
            mProfileData obj = new mProfileData();
            obj.EmplId = emplId;
            return objDAL.GetProfileData(obj, "GET_DETAILS");
        }

        public DataSet GetInvoiceSummaryDetail(string emplId, string quarter, string location, string year)
        {
            mInvoiceSummaryDetail obj = new mInvoiceSummaryDetail();
            obj.EmplId = emplId;
            obj.Quarters = quarter;
            obj.Location = location;
            obj.Year = year;
            return objDAL.InvoiceSummaryData(obj, "S1P");
        }

        public DataSet GetInvoiceSummary(string EmplID, string quarter, string year)
        {
            mInvoiceSummaryDetail obj = new mInvoiceSummaryDetail();
            obj.EmplId = EmplID;
            obj.Quarters = quarter;
            obj.Year = year;
            return objDAL.InvoiceSummaryData(obj, "S1L");
        }

        public DataSet GetInvoiceExcel(string EmplContID, string FiscalYearID)
        {
            mInvoiceSummaryDetail obj = new mInvoiceSummaryDetail();
            obj.EmplId = EmplContID;
            obj.FiscalYearID = FiscalYearID;
            return objDAL.InvoiceExcelExport(obj, "EXCEL");
        }
    }
}