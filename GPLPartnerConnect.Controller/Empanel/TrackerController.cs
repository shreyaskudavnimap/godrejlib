﻿using GPLPartnerConnect.DAL.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GPLPartnerConnect.Controller.Empanel
{
    public class TrackerController
    {
        TrackerDAL objDAL = new TrackerDAL();

        public void ProjectTracker(mTracker tracker)
        {
            objDAL.PushProjectTracker(tracker, "PROJ");
        }
        public void NotificationTracker(mTracker tracker)
        {
            objDAL.PushNotificationTracker(tracker, "NOTIF");
        }
    }
}
