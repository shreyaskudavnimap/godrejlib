﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Controller.Master;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using GPLPartnerConnect.Model.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for ChangePassword
    /// </summary>
    public class GenericFileUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            EmpanelmentController objcon = new EmpanelmentController();
            context.Response.ContentType = "text/plain";
            var res = context.Request.Form["Files"];
            mCommonOutput o = new mCommonOutput();

            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                jss.MaxJsonLength = Int32.MaxValue;
                List<UFile> files = jss.Deserialize<List<UFile>>(res);
                //string folder = System.Configuration.ConfigurationManager.AppSettings["AttachmentPhysicalPath"];
                string path = AppDomain.CurrentDomain.BaseDirectory + System.Configuration.ConfigurationManager.AppSettings["AttachmentPhysicalPath"];

                foreach (var file in files)
                {
                    File.WriteAllBytes(path + file.file_name, Convert.FromBase64String(file.file_base64_encoded.Replace("\"", "").Trim()));
                }
                o.status = Status.Success.ToString();
                o.statusCode = Convert.ToInt32(Status.Success);
            }
            catch(Exception ex)
            {
                o.status = ex.Message.ToString();
                o.statusCode = Convert.ToInt32(Status.Error);
            }
            
            context.Response.Write(Common.JSONSerialize(o));
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

   

    public class UFile
    {
        public string file_base64_encoded { get; set; }
        public string file_name { get; set; }
    }
}