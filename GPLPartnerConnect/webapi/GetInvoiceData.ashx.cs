﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetBookingData
    /// </summary>
    public class GetInvoiceData : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            InvoiceController objCon = new InvoiceController();

            string EmplId = context.Request["userid"];
            string quarter = context.Request["quarter"];
            string ProjectId = context.Request["sfdcprojectid"];
            string year = context.Request["year"];
            DataSet ds = objCon.GetInvoiceData(EmplId, quarter, ProjectId, year);
            List<mInvoiceData> list = Common.ToObject<mInvoiceData>(ds.Tables[0]);

            //mInvoices obj = new mInvoices();
            //obj.EmplId = context.Request["userid"];
            //obj.Quarters = context.Request["quarter"];
            //DataSet ds = objCon.GetRegionList(obj);
            //List<mRegions> list = Common.ToObject<mRegions>(ds.Tables[0]);

            o.data = list;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class mRegions
        {
            public string SFDC_Region { get; set; }
        }
    }
}