﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetEnquiryData
    /// </summary>
    public class GetEOIOTP : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            BrokerSFDCDataController objCon = new BrokerSFDCDataController();
            //string EmplContID = context.Request["userid"];

            string module = context.Request["module"];
            switch (module)
            {
                case "send_otp":
                    SendOTP(context);
                    break;

                case "submit_otp":
                    ValidateOTP(context);
                    break;
            }
        }

        private void ValidateOTP(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            EOIController objCon = new EOIController();
            mEOI obj = new mEOI();
            obj.EmplId = context.Request["userid"];
            obj.Mobile = context.Request["mobile"];
            obj.OTPNo = context.Request["OTP"];
            string resp = objCon.ValidateOTP(obj);
            o.data = resp;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        private void SendOTP(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            EOIController objCon = new EOIController();
            mEOI obj = new mEOI();
            obj.EmplId = context.Request["userid"];
            obj.Mobile = context.Request["mobile"];
            string resp = objCon.SendOTP(obj);
            o.data = resp;
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}