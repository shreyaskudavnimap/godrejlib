﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetEnquiryData
    /// </summary>
    public class TrackerAPI : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string flag = context.Request["flag"];

            switch (flag)
            {
                case "project":
                    pushProjectTracker(context);
                    break;

                case "notification":
                    pushNotificationTracker(context);
                    break;
            }
        }

        private void pushNotificationTracker(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            TrackerController objContr = new TrackerController();
            mTracker tracker = new mTracker();

            tracker.UserName = context.Request["username"];
            tracker.JsonData = context.Request["jsondata"];
            tracker.Identifier = context.Request["identifier"];
            tracker.NotificationId = context.Request["notificationId"];

            objContr.NotificationTracker(tracker);

            o.data = "success";
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        private void pushProjectTracker(HttpContext context)
        {
            mCommonOutput o = new mCommonOutput();
            TrackerController objContr = new TrackerController();
            mTracker tracker = new mTracker();

            tracker.UserName = context.Request["username"];
            tracker.JsonData = context.Request["jsondata"];
            tracker.Identifier = context.Request["identifier"];
            tracker.Module = context.Request["module"];

            objContr.ProjectTracker(tracker);

            o.data = "success";
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}