﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetBookingSummary
    /// </summary>
    public class GetInvoiceSummary : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string userid = context.Request["userid"];
            string quarter = context.Request["quarter"];
            string year = context.Request["year"];
            InvoiceController obj = new InvoiceController();

            DataSet ds = obj.GetInvoiceSummary(userid, quarter, year);
            List<mInvoiceSummary> o = new List<mInvoiceSummary>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                mInvoiceSummary ivs = new mInvoiceSummary();
                ivs.EmplId = row["EmplID"].ToString();
                ivs.Location = row["Location"].ToString();
                ivs.TotalPaid = row["TotalPaid"].ToString();
                ivs.InvoiceInProgress = row["InvoiceInProgress"].ToString();
                ivs.TotalBilled = row["TotalBilled"].ToString();
                DataSet ds2 = obj.GetInvoiceSummaryDetail(userid, quarter, ivs.Location, year);
                foreach (DataRow row2 in ds2.Tables[0].Rows)
                {
                    mInvoiceSummaryDetail bsd = new mInvoiceSummaryDetail();
                    bsd.EmplId = row2["EmplID"].ToString();
                    bsd.ProjectId = row2["ProjectId"].ToString();
                    bsd.Project = row2["Project"].ToString();
                    bsd.Location = row2["Location"].ToString();
                    bsd.InvoiceInProgress = row2["InvoiceInProgress"].ToString();
                    bsd.TotalBilled = row2["TotalBilled"].ToString();
                    bsd.TotalPaid = row2["TotalPaid"].ToString();
                    ivs.detail.Add(bsd);
                }
                if (ivs.Location == "")
                    ivs.Location = "NULL";
                o.Add(ivs);
            }

            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}