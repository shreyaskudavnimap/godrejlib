﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for InsertEmpanelment
    /// </summary>
    public class InsertEmpanelment : IHttpHandler
    {

        EmpanelmentController objcon = new EmpanelmentController();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string jsonempl = context.Request["empl"];
            WLog.WriteLog("jsonempl=" + jsonempl, "InsertEmpanelment");
            mEmpanelment empl = Common.JSONDeSerialize<mEmpanelment>(jsonempl);
            context.Response.Write(Common.JSONSerialize(InsertEmpl(empl)));
        }

        mCommonOutput InsertEmpl(mEmpanelment empl) 
        {
            mCommonOutput obj = objcon.InsertEmpanelment(empl);
            
            return obj;
            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}