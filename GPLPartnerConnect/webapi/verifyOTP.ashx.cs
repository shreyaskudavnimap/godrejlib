﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for verifyOTP
    /// </summary>
    public class verifyOTP : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string EmplContID = context.Request["userid"];
            string pin = context.Request["otp"];
            mCommonOutput o = new mCommonOutput();
            OTPController otpCon = new OTPController();
            if(otpCon.verifyOTP(EmplContID, pin))
            {
                o.status = Status.Success.ToString();
                o.statusCode = Convert.ToInt32(Status.Success);
                o.msg = "Success";
            }
            else
            {
                o.status = Status.Fail.ToString();
                o.statusCode = Convert.ToInt32(Status.Fail);
                o.msg = "InValid";

            }

            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}