﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetOTPForMenu
    /// </summary>
    public class GetOTPForMenu : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            EmpanelmentController empController = new EmpanelmentController();
            mCommonOutput o = new mCommonOutput();
            OTPController otpCon = new OTPController();
            string EmplContID = context.Request["userid"];
            EmailService emailService = new EmailService();
            //try
            //{
                if (o != null)
                {
                    mOTP otp = otpCon.ExistingOtp(EmplContID);
                    mEmpanelmentContact emp = empController.GetEmpanelmentContactDetails(EmplContID);

                    if (otp == null)
                    {
                        otp = new mOTP();
                        otp.pin = otpCon.SendOTP(emp, "117").ToString();
                        emailService.properties.AddToEmail(emp.Email);
                        otpCon.SendEmail(otp.pin, emp);
                        o.status = Status.Success.ToString();
                        o.statusCode = Convert.ToInt32(Status.Success);
                        o.msg = "Success";
                    }
                    else
                    {
                        otpCon.SendEmail(otp.pin, emp);
                        o.status = Status.Success.ToString();
                        o.statusCode = Convert.ToInt32(Status.Success);
                        o.msg = "Success";
                    }
                }
                else
                {
                    o.status = Status.Fail.ToString();
                    o.statusCode = Convert.ToInt32(Status.Fail);
                    o.msg = "Broker ID Not found";
                }
            //}
            //catch (Exception ex)
            //{
            //    o.status = Status.Error.ToString();
            //    o.statusCode = Convert.ToInt32(Status.Error);
            //    o.msg = ex.ToString();
            //}
            
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}