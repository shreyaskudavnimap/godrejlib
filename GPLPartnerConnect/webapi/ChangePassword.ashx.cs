﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Controller.Master;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using GPLPartnerConnect.Model.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for ChangePassword
    /// </summary>
    public class ChangePassword : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            EmpanelmentController EmpCon = new EmpanelmentController();
            context.Response.ContentType = "text/plain";
            string EmplContID = context.Request["userid"];
            string OldPassword = context.Request["old_password"];
            string NewPassword = context.Request["new_password"];
            mCommonOutput o = new mCommonOutput();
            mEmpanelmentContact con = EmpCon.GetEmpanelmentContactDetails(EmplContID);
            if(con != null)
            {
                if (con.password != OldPassword)
                {
                    o.status = Status.Fail.ToString();
                    o.statusCode = Convert.ToInt32(Status.Fail);
                    o.msg = "Please enter correct password";
                }
                else
                {
                    EmpCon.UpdatePassword(EmplContID, NewPassword);
                    o.status = Status.Success.ToString();
                    o.statusCode = Convert.ToInt32(Status.Success);
                    o.msg = "Password successfully changed";

                    EmailService es = new EmailService();
                    MailTemplateController mailCont = new MailTemplateController();
                    mMailTemplate mail = mailCont.GetMailTempleteByCode("PASSCH");
                    string email = con.Email;
                    string name = con.Name;
                    es.properties.AddToEmail(email, name);

                    es.properties.MailSubject = mail.Subject;
                    string Body = mail.Body;
                    Body = Body.Replace("#EmpanelmentName#", name);
                    es.properties.MailBody = Body;
                    es.SendMail();
                }
            }
            else
            {
                o.status = Status.Fail.ToString();
                o.statusCode = Convert.ToInt32(Status.Fail);
                o.msg = "Broker ID Not found";
            }
            context.Response.Write(Common.JSONSerialize(o));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}