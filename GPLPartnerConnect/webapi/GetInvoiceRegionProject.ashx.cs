﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;

namespace GPLPartnerConnect.webapi
{
    /// <summary>
    /// Summary description for GetBookingData
    /// </summary>
    public class GetInvoiceRegionProject : IHttpHandler
    {
        public string _downloadUrl = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            mCommonOutput o = new mCommonOutput();
            InvoiceController objCon = new InvoiceController();
            mInvoices obj = new mInvoices();
            string flag = context.Request["flag"];
            obj.EmplId = context.Request["userid"];
            obj.Quarters = context.Request["quarter"];

            if (flag == "region")
            {
                DataSet ds = objCon.GetRegionList(obj);
                List<mRegions> rlist = Common.ToObject<mRegions>(ds.Tables[0]);
                o.data = rlist;
            }

            else if(flag == "project")
            {
                obj.Region = context.Request["region"];
                DataSet ds = objCon.GetProjectList(obj);
                List<mProjects> plist = Common.ToObject<mProjects>(ds.Tables[0]);
                o.data = plist;
            }

            else if (flag == "attachments")
            {
                obj.Region = context.Request["region"];
                obj.Project = context.Request["project"];
                DataSet ds = objCon.GetProjectInvoices(obj);
                List<mAttachmentList> attachLst = Common.ToObject<mAttachmentList>(ds.Tables[0]);
                if (attachLst.Count != 0)
                {
                    List<string> files = GenerateFiles(attachLst);
                    string zipPath = CreateZipFile(files);
                    o.data = _downloadUrl + zipPath;
                }
                else
                {
                    o.data = "no_files";
                }
            }
            o.status = Status.Success.ToString();
            o.statusCode = Convert.ToInt32(Status.Success);
            context.Response.Write(Common.JSONSerialize(o));
        }

        public List<string> GenerateFiles(List<mAttachmentList> attachLst)
        {
            List<string> result = new List<string>();
            foreach (var attach in attachLst)
            {
                //string filename = attach.SFDC_PropStrengthPrimaryApplicantNameC.Replace(" ","").Replace(".", "")
                //    + "_" + GetPropertyName(attach) + ".pdf";
                string filepath = System.Web.Hosting.HostingEnvironment.MapPath("/data/invoices/temp_files/" + attach.FILENAME.Replace("(0)","") + ".pdf");
                DeleteFile(filepath);
                byte[] bytes = Convert.FromBase64String(attach.AttachmentBody);
                System.IO.FileStream stream = new FileStream(filepath, FileMode.Create);
                System.IO.BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(bytes, 0, bytes.Length);
                writer.Close();
                result.Add(filepath);
            }
            return result;
        }

        private string GetPropertyName(mAttachmentList attach)
        {
            string result = String.Empty;
            if(String.IsNullOrEmpty(attach.SFDC_cip_tower) && String.IsNullOrEmpty(attach.SFDC_cip_tower) && String.IsNullOrEmpty(attach.SFDC_cip_tower))
            {
                result = "NA";
            }
            else
            {
                result = attach.SFDC_cip_tower + attach.SFDC_cip_tower + attach.SFDC_cip_tower;
            }
            return result;
        }

        public string CreateZipFile(IEnumerable<string> files)
        {
            string filename = "Invoices_" + DateTime.Now.ToString("yyyyMMddhmmss") + ".zip";
            string filePath = System.Web.Hosting.HostingEnvironment.MapPath("/data/invoices/zip_files/" + filename);
            
            // Create and open a new ZIP file
            var zip = ZipFile.Open(filePath, ZipArchiveMode.Create);
            foreach (var file in files)
            {
                // Add the entry for each file
                zip.CreateEntryFromFile(file, Path.GetFileName(file), CompressionLevel.Optimal);
            }
            // Dispose of the object when we are done
            zip.Dispose();

            return filename;
        }

        private void DeleteFile(string filepath)
        {
            System.IO.File.Delete(filepath);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class mRegions
        {
            public string SFDC_Region { get; set; }
        }

        public class mAttachmentList
        {
            public string SFDC_PropStrengthPrimaryApplicantNameC { get; set; }
            public string SFDC_cip_tower { get; set; }
            public string SFDC_Wing_Block { get; set; }
            public string SFDC_house_unit { get; set; }
            public string AttachmentBody { get; set; }
            public string FILENAME { get; set; }
        }
        public class mProjects
        {
            public string Project { get; set; }

            public string Location { get; set; }

            public string EmplID { get; set; }

            public string EmplContID { get; set; }

            public string ProjectID { get; set; }
        }
    }
}