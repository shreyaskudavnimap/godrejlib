﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace GPLPartnerConnect.include
{
    public partial class admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
                Response.Redirect("AdminLogin.aspx");
            if (Convert.ToString(Session["type"]) == "admin")
            {
                HtmlControl User = (HtmlControl)Menu.FindControl("UserLi");
                User.Visible = false;
                HtmlControl Launch = (HtmlControl)Menu.FindControl("BannerLi");
                Launch.Visible = false;
                HtmlControl Master = (HtmlControl)Menu.FindControl("MasterLi");
                Master.Visible = false;
                HtmlControl Projects = (HtmlControl)Menu.FindControl("ProjectsLi");
                Projects.Visible = false;
                HtmlControl Enquiry = (HtmlControl)Menu.FindControl("EnquiryLi");
                Enquiry.Visible = false;
                HtmlControl Events = (HtmlControl)Menu.FindControl("EventsLi");
                Events.Visible = false;
                HtmlControl News = (HtmlControl)Menu.FindControl("NewsLi");
                News.Visible = false;
                HtmlControl Offers = (HtmlControl)Menu.FindControl("OffersLi");
                Offers.Visible = false;
                HtmlControl Property = (HtmlControl)Menu.FindControl("PropertyLi");
                Property.Visible = false;
                HtmlControl SubAdmin = (HtmlControl)Menu.FindControl("SubAdminLi");
                SubAdmin.Visible = false;
                //HtmlControl PwdAnchor = (HtmlControl)Menu.FindControl("PwdLi");
                //PwdAnchor.Visible = false;
                HtmlControl DataAnchor = (HtmlControl)Menu.FindControl("DataDetailsLi");
                DataAnchor.Visible = false;
            }

        }
    }
}