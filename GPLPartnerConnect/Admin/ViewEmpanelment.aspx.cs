﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPLPartnerConnect.Admin
{
    public partial class ViewEmpanelment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
                Response.Redirect("AdminLogin.aspx");
        }
    }
}