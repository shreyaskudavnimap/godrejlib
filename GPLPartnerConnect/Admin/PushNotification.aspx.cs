﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.FCM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPLPartnerConnect.Admin
{
    public partial class PushNotification : System.Web.UI.Page
    {
        NotificationController objCon = new NotificationController();
        string userid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
                Response.Redirect("AdminLogin.aspx");

            userid = Convert.ToString(Session["User"]);
            if (!IsPostBack)
            {
                BindGrid();
            }

        }


        private void BindGrid()
        {
            divgrid.Visible = true;
            divDetail.Visible = false;
            DataSet ds = objCon.GetNotificationAll();
            grView.DataSource = ds.Tables[0];
            grView.DataBind();

        }

        protected void grView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grView.PageIndex = e.NewPageIndex;
            BindGrid();
        }



        protected void grView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                lblID.Text = e.CommandArgument.ToString();
                Showfrom(lblID.Text);
            }
            else if (e.CommandName == "Send")
            {
                SendNotification(e.CommandArgument.ToString());
            }
        }

        void SendNotification(string id)
        {
            DataSet ds = objCon.GetNotificationByID(id);
            string str = objCon.SendNotification(ds.Tables[0], userid);
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('" + str+  " Notification Send Successfully'); window.location='PushNotification.aspx';</script>");



        }
        void Showfrom(string id)
        {
            DataSet ds = objCon.GetNotificationByID(id);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                divDetail.Visible = true;
                divgrid.Visible = false;
                spTitle.InnerText = "View";
                if (row["HasSend"].ToString() == "Y")
                {
                    txtNotificationBody.Enabled = false;
                    txtNotificationTitle.Enabled = false;
                    txtSubTitle.Enabled = false;
                    btnSubmit.Visible = false;
                }
                 else
                {
                    txtNotificationBody.Enabled = true;
                    txtNotificationTitle.Enabled = true;
                    txtSubTitle.Enabled = true;
                }
                txtNotificationBody.Text = row["NotificationBody"].ToString();
                txtNotificationTitle.Text = row["NotificationTitle"].ToString();
                txtSubTitle.Text = row["SubTitle"].ToString();

               
            }
            else
            {
                Response.Redirect("PushNotification.aspx");
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            divDetail.Visible = true;
            divgrid.Visible = false;
            lblID.Text = "0";
            spTitle.InnerText = "Add";
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Model.Empanel.mNotification obj = new Model.Empanel.mNotification();
            obj.NotificationID = lblID.Text;
            obj.NotificationTitle = txtNotificationTitle.Text;
            obj.NotificationBody = txtNotificationBody.Text;
            obj.SubTitle = txtSubTitle.Text;
            obj.IsRedirect = "N";
            obj.HasSend = "N";
            obj.ChannelType = "notification";

            obj.NotificationType = "M";
            obj.MobileUrl = "";
            obj.ChannelID = "0";
            obj.Active = "Y";
            obj.InsertedBy = userid;

            objCon.InsertUpdateNotification(obj);
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record Saved Successfully'); window.location='PushNotification.aspx';</script>");
        }
    }
}