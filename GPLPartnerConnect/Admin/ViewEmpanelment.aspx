﻿<%@ Page Title="" Language="C#" MasterPageFile="~/include/admin.Master" AutoEventWireup="true" CodeBehind="ViewEmpanelment.aspx.cs" Inherits="GPLPartnerConnect.Admin.ViewEmpanelment" %>

<%@ Register Src="control/MenuControl.ascx" TagName="MenuControl" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <%--<div role="main" id="main" class="container_12 clearfix">--%>
        <!-- The blue toolbar stripe -->
        <%--<section class="toolbar">
            <asp:TextBox ID="txtSearch" onfocus="clearDefault(this)" Text="search by name" onblur="restore(this)" runat="server" CssClass="tooltip"></asp:TextBox>
            <asp:Button ID="btnSubmit" runat="server" CssClass="submit go" Text="Go"
            onclick="btnSubmit_Click" />
        </section>--%>
        <!-- End of .toolbar-->
        <!-- The sidebar -->
        <%--<uc:MenuControl id="Menu" runat="server" />--%>
        <!-- End of sidebar -->
        <!-- Here goes the content. -->
        <%--<section id="content" class="container_12 clearfix" data-sort="true">--%>
            <h1 class="grid_12">
                View Empanelment</h1>
            <div class="grid_12">
                <div class="box">
                    <div id="divGrid" runat="server">
                        <div class="content">
                            <div class="tabletools">
                                <div class="left">
                                <%--<asp:LinkButton ID="lnkBtnAddContent" runat="server" ToolTip="Add Project"
                                CssClass="open-add-client-dialog" OnClick="lnkBtnAddContent_Click"><i class="icon-plus"></i>Update Code</asp:LinkButton>--%>
                                    <%--<a class="open-add-client-dialog" href="add-projectdetails.aspx"><i class="icon-plus">
                                    </i>Add Project</a>--%>
                                </div>
                                <div class="right">
                                </div>
                            </div>
                            <asp:GridView ID="grdDetails" runat="server" AutoGenerateColumns="false" DataKeyNames="EmplID"
                                AllowPaging="true" PageSize="10" OnPageIndexChanging="grdDetails_PageIndexChanging"
                                OnRowCommand="grdDetails_RowCommand" OnRowDataBound="grdDetails_RowDataBound"
                                CssClass="dynamic styled with-prev-next">
                                <Columns>
                                    <asp:TemplateField HeaderText="Requested On">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                        <ItemTemplate>
                                            <%# Eval("RequestUpdatedOn") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                        <ItemTemplate>
                                            <%# Eval("FirstName") + " " + Eval("LastName") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:BoundField HeaderText="Email" DataField="Email">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                     <asp:BoundField HeaderText="Mobile No" DataField="Mobile">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                     <asp:BoundField HeaderText="Pan No" DataField="Pan_No">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatusText" runat="server" />
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Registered On">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                        <ItemTemplate>
                                            <%# Eval("RegisteredOn") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnView" Text="View" runat="server" CommandName="View"
                                                CommandArgument='<%# Eval("EmplID") %>'></asp:LinkButton>
                                            <%--<asp:LinkButton ID="lnkBtnEdit" Text="View Gst" runat="server" CommandName="ViewGst"
                                                CommandArgument='<%# Eval("EmplID") %>'></asp:LinkButton>
                                            |
                                            <asp:LinkButton ID="LinkButton1" Text="View Rera" runat="server" CommandName="ViewRera"
                                                CommandArgument='<%# Eval("EmplID") %>'></asp:LinkButton>
                                            |
                                            <asp:LinkButton ID="lnkPushSales" Text="Register" runat="server" CommandName="PushSales"
                                                CommandArgument='<%# Eval("EmplID") %>'></asp:LinkButton>--%>
                                            
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Label ID="lblMsg" runat="server" Text="" Visible="false"></asp:Label>
                            
                        
                        </div>
                        <!-- End of .content -->
                    </div>
                    <!-- End of #divGrid -->
                    <div id="divForm" runat="server">
                        <div class="header">
                            <h2>
                                Empanelment Details</h2>
                            <asp:Label ID="lblEmplID" runat="server"  Visible="false"></asp:Label>
                            <asp:Label ID="Label1" runat="server"  Visible="false"></asp:Label>
                        </div>
                        <div class="content">
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Empanelment ID</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblEmpanelmentID" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Company Name</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblCompany_Name" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">EntityType</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblEntityType" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">FirstName</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">LastName</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblLastName" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Mobile</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblMobile" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Email</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Pan No</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblPan_No" runat="server"></asp:Label>
                                        <a id="aPan_No" runat="server" target="_blank" >View Doc</a>
                                    </div>
                                    <div>
                                        <%--<asp:LinkButton ID="lnkBtnApprove" Text="Approve" runat="server" CommandName="Approve"
                                                ></asp:LinkButton>
                                            |
                                                        <asp:LinkButton ID="lnkBtnReject" Text="Reject" runat="server" CommandName="Reject"
                                                            ></asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Broker Account ID</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblBrokerAccountID" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Billing Street</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblBilling_Street" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Billing City</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblBilling_City" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Billing Zip</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblBilling_Zip" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Billing State</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblBilling_State" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Billing Country</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblBilling_Country" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered Street</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblRegistered_Street" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered City</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblRegistered_City" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered Zip</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblRegistered_Zip" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered State</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblRegistered_State" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Registered Country</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblRegistered_Country" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication Street</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblCommunication_Street" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication City</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblCommunication_City" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication Zip</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblCommunication_Zip" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication State</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblCommunication_State" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Communication Country</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblCommunication_Country" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="full validate">
                                <div class="row">
                                    <label for="txtTitle">
                                        <strong style="width: 121px;">Status</strong>
                                    </label>
                                    <div class="Label">
                                        <asp:Label ID="lblDisplayStatus" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of .content -->
                        <div class="header">
                            <h2>
                                View Rera Details</h2>
                        </div>
                        <div class="content">
                            <asp:GridView ID="gdvRera" runat="server" AutoGenerateColumns="false" DataKeyNames="ID"
                                AllowPaging="true" PageSize="10" OnRowCommand="gdvRera_RowCommand" OnRowDataBound="gdvRera_RowDataBound"
                                CssClass="dynamic styled with-prev-next" OnPageIndexChanging="gdvRera_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField HeaderText="State" DataField="State">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Rera No" DataField="Rera_no">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Rera Certi" DataField="Rera_Certi">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Status" DataField="Status">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Comment">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Text='<%#Eval("Comment") %>'></asp:TextBox>
                                            <asp:Label ID="lblComment" runat="server"  Text='<%#Eval("Comment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("Status") %>' Visible="false"></asp:Label>
                                            <a href='<%#"/data/empanelment/"+Eval("Rera_Certi")%>' target="_blank" >View Doc</a>
                                            |
                                            <asp:LinkButton ID="lnkBtnApprove" Text="Approve" runat="server" CommandName="Approve"
                                                CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                            |
                                                        <asp:LinkButton ID="lnkBtnReject" Text="Reject" runat="server" CommandName="Reject"
                                                            CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Label ID="lblMsgRera" runat="server" Text="" Visible="false"></asp:Label>

                        </div>
                        <!-- End of .content -->
                        <div class="header">
                            <h2>
                                View Gst Details</h2>
                        </div>
                        <div class="content">
                            <asp:GridView ID="gdvGst" runat="server" AutoGenerateColumns="false" DataKeyNames="ID"
                                AllowPaging="true" PageSize="10" OnRowCommand="gdvGst_RowCommand" OnRowDataBound="gdvGst_RowDataBound"
                                CssClass="dynamic styled with-prev-next" OnPageIndexChanging="gdvGst_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField HeaderText="State" DataField="State">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="GST No" DataField="Gst_no">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="GST Certi" DataField="Gst_Certi">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Status" DataField="Status">
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Comment">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Text='<%#Eval("Comment") %>'></asp:TextBox>
                                            <asp:Label ID="Label3" runat="server"  Text='<%#Eval("Comment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("Status") %>' Visible="false"></asp:Label>
                                            <a href='<%#"/data/empanelment/"+Eval("Gst_Certi")%>' target="_blank" >View Doc</a>
                                            |
                                            <asp:LinkButton ID="LinkButton1" Text="Approve" runat="server" CommandName="Approve"
                                                CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                            |
                                            <asp:LinkButton ID="LinkButton2" Text="Reject" runat="server" CommandName="Reject"
                                                CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="table-header-repeat line-left minwidth-1" />
                                        <ItemStyle CssClass="icon-1 info-tooltip" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Label ID="lblMsgGst" runat="server" Text="" Visible="false"></asp:Label>
                            <div class="actions">
                                <div class="right">
                                    <asp:Button ID="btnSendRejectMail" runat="server" Visible="false"
                                        CssClass="submit" Text="Send Rejection Mail" OnClick="btnSendRejectMail_Click"  />
                                    <asp:Button ID="btnRegister" runat="server"
                                        CssClass="submit" Text="Register" OnClick="btnRegister_Click"  />
                                    <asp:Button ID="btnCancel" runat="server"
                                        CssClass="submit" Text="Cancel" OnClick="btnCancel_Click"  />
                                </div>
                            </div>
                        </div>
                        <!-- End of .content -->

                    </div>
                    <!-- End of #divForm -->

                </div>
                <!-- End of .box -->
            </div>
        <%--</section>--%>
        <!-- End of #content -->
    <%--</div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
