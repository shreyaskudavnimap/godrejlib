﻿<%@ Page Title="" Language="C#" MasterPageFile="~/include/admin.Master" AutoEventWireup="true" CodeBehind="EventNotification.aspx.cs" Inherits="GPLPartnerConnect.Admin.EventNotification" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <script src="js/mylibs/forms/jquery.ui.datetimepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#<%=txtNotificationScheduledFor.ClientID %>").datetimepicker({
                dateFormat: 'yy-mm-dd',
                timeFormat: 'hh:mm:ss'                
            }).attr('readonly', 'readonly');;
        });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
     <h4>Event Notification <span id="spTitle" runat="server"></span></h4>
     <div id="divDetail" runat="server">
        <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblEventID" runat="server" Visible="false"></asp:Label>

         <div class="form-horizontal">
            Notification Title<br />
            <br />
            <asp:TextBox ID="txtNotificationTitle" runat="server" /> 
            <asp:RequiredFieldValidator ID="txtTitle" runat="server" ControlToValidate="txtNotificationTitle" ErrorMessage="Required" 
                     ForeColor="Red"></asp:RequiredFieldValidator>  
            <br />
            Notification SubTitle<br />
            <br />
            <asp:TextBox ID="txtNotificationSubTilte" runat="server" /> 
            <asp:RequiredFieldValidator ID="txtSubTilte" runat="server" ControlToValidate="txtNotificationSubTilte" ErrorMessage="Required" 
                     ForeColor="Red"></asp:RequiredFieldValidator>
            <br />

            Notification Schedule<br />
            <br />
            <asp:TextBox ID="txtNotificationScheduledFor" runat="server" /> 
            <asp:RequiredFieldValidator ID="txtScheduledFor" runat="server" ControlToValidate="txtNotificationScheduledFor" ErrorMessage="Required" 
                     ForeColor="Red"></asp:RequiredFieldValidator>
            <br />
            
            
            <asp:Button ID="btnSubmit" runat="server"
                CssClass="submit" Text="Submit" OnClick="btnSubmit_Click" />
             

             <asp:Button ID="btnCancel" runat="server"  CausesValidation="false"
                CssClass="Cancel" Text="Cancel" OnClick="btnCancel_Click" />
        </div>
    </div>
    
    <div id="divgrid" runat="server">
       <%-- <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />--%>
        
        <asp:GridView ID="grView" runat="server" AutoGenerateColumns="false" DataKeyNames="EventNotificationID"
            AllowPaging="true" PageSize="10" OnPageIndexChanging="grView_PageIndexChanging"
            OnRowCommand="grView_RowCommand" Width="100%">
             <Columns>
                <asp:BoundField DataField="NotificationTitle" HeaderText="Title" />
                <asp:BoundField DataField="NotificationSubTilte" HeaderText="Subtitle" />
                 <asp:BoundField DataField="NotificationScheduledFor" HeaderText="Schedule For" />
                <asp:BoundField DataField="NotificationSendOn" HeaderText="Send On" />
                


                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkBtnEdit" Text="View" runat="server" CommandName="View" CausesValidation="false"
                            CommandArgument='<%# Eval("EventNotificationID") %>'></asp:LinkButton>                        
                         
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

   

   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
