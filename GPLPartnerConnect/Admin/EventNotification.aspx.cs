﻿using GPLPartnerConnect.CommonUtility;
using GPLPartnerConnect.Controller.Empanel;
using GPLPartnerConnect.Model.Empanel;
using GPLPartnerConnect.Model.FCM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPLPartnerConnect.Admin
{
    public partial class EventNotification : System.Web.UI.Page
    {
        EventNotificationController objCon = new EventNotificationController();
         
        string userid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["User"])))
                Response.Redirect("AdminLogin.aspx");

            userid = Convert.ToString(Session["User"]);
            if(Request.QueryString["EID"] != null)
            {
                lblEventID.Text = Request.QueryString["EID"];
            }
            else
            {
                Response.Redirect("viewevents.aspx");
            }
            if (!IsPostBack)
            {
                BindGrid();
            }

        }


        private void BindGrid()
        {
            DataSet ds = objCon.GetNotificationAll(lblEventID.Text);
            grView.DataSource = ds.Tables[0];
            grView.DataBind();
        }

        protected void grView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grView.PageIndex = e.NewPageIndex;
            BindGrid();
        }



        protected void grView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                lblID.Text = e.CommandArgument.ToString();
                Showfrom(lblID.Text);
            }

        }

        void Showfrom(string id)
        {
            DataSet ds = objCon.GetNotificationByID(id);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                //divDetail.Visible = true;
                //divgrid.Visible = false;
                spTitle.InnerText = "View";
                if (row["NotificationHasSend"].ToString() == "Y")
                {

                    txtNotificationTitle.Enabled = false;
                    txtNotificationSubTilte.Enabled = false;
                    txtNotificationScheduledFor.Enabled = false;
                    btnSubmit.Visible = false;
                }
                else
                {

                    txtNotificationTitle.Enabled = true;
                    txtNotificationSubTilte.Enabled = true;
                    txtNotificationScheduledFor.Enabled = true;
                }

                txtNotificationTitle.Text = row["NotificationTitle"].ToString();
                txtNotificationSubTilte.Text = row["NotificationSubTilte"].ToString();
                txtNotificationScheduledFor.Text = Convert.ToDateTime(row["NotificationScheduledFor"].ToString()).ToString("yyyy-MM-dd hh:mm:ss");


            }


            else
            {
                Response.Redirect("EventNotification.aspx?EID=" + lblEventID.Text);
            }
        }

        //protected void btnAdd_Click(object sender, EventArgs e)
        //{
        //    divDetail.Visible = true;
        //    divgrid.Visible = false;
        //    lblID.Text = "0";
        //    spTitle.InnerText = "Add";
        //}


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            mEventNotification obj = new mEventNotification();
            obj.EventNotificationID = lblID.Text;
            obj.EventID = lblEventID.Text;
            obj.NotificationTitle = txtNotificationTitle.Text;
            obj.NotificationSubTilte = txtNotificationSubTilte.Text;
            obj.NotificationScheduledFor = txtNotificationScheduledFor.Text;

            obj.Active = "Y";
            obj.InsertedBy = userid;
            if (Page.IsValid)
            {
                lblID.Text = "Required field is filled!";
            }
            else
            {
                lblID.Text = "Required field is empty!";
            }

            objCon.InsertUpdateNotification(obj);
            ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record Saved Successfully'); window.location='EventNotification.aspx?EID=" + lblEventID.Text + "';</script>");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("EventNotification.aspx?EID=" + lblEventID.Text);

            // ClientScript.RegisterStartupScript(GetType(), "js", "<script>alert('Record Cancel Successfully'); window.location='ProjectNotification.aspx?PID=" + lblProjectID.Text + "';</script>");
        }
    }
}