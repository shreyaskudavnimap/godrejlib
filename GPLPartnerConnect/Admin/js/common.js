﻿// To clear default value 
function clearDefault(ele) {
    if (ele.defaultValue == ele.value) {
        ele.value = ''
    }
}

// To restore default value 
function restore(ele) {
    if (ele.value == '') {
        ele.value = ele.defaultValue;
    }
}