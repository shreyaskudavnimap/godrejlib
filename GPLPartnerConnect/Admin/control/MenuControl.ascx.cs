﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPLPartnerConnect.Admin.control
{
    public partial class MenuControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["type"]) == "admin")
            {
                UserLi.Visible = false;
                BannerLi.Visible = false;
                MasterLi.Visible = false;
                ProjectsLi.Visible = false;
                EnquiryLi.Visible = false;
                EventsLi.Visible = false;
                NewsLi.Visible = false;
                OffersLi.Visible = false;
                PropertyLi.Visible = false;
                SubAdminLi.Visible = false;
                //HtmlControl PwdAnchor = (HtmlControl)Menu.FindControl("PwdLi");
                //PwdAnchor.Visible = false;
                DataDetailsLi.Visible = false;
            }
        }
    }
}