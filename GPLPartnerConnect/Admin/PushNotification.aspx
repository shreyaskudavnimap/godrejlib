﻿<%@ Page Title="" Language="C#" MasterPageFile="~/include/admin.Master" AutoEventWireup="true" CodeBehind="PushNotification.aspx.cs" Inherits="GPLPartnerConnect.Admin.PushNotification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div id="divgrid" runat="server">
        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
        <asp:GridView ID="grView" runat="server" AutoGenerateColumns="false" DataKeyNames="NotificationID"
            AllowPaging="true" PageSize="10" OnPageIndexChanging="grView_PageIndexChanging"
            OnRowCommand="grView_RowCommand" Width="100%">
            <Columns>
                <asp:BoundField DataField="NotificationTitle" HeaderText="Title" />
                <asp:BoundField DataField="SubTitle" HeaderText="Subtitle" />
                <asp:BoundField DataField="NotificationBody" HeaderText="Body" />
                <asp:BoundField DataField="nSendOn" HeaderText="SendOn" />
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkBtnView" Text="View" runat="server" CommandName="View"
                            CommandArgument='<%# Eval("NotificationID") %>'></asp:LinkButton>|                         
                        <asp:LinkButton ID="lnkBtnSend" Text="Send" runat="server" CommandName="Send"
                            CommandArgument='<%# Eval("NotificationID") %>' Visible='<%# Convert.ToBoolean(Eval("nHasSend")) %>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div id="divDetail" runat="server">
        <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>

        <h4>Push Notification <span id="spTitle" runat="server"></span></h4>
        <div class="form-horizontal">
            Notification Title<br />
            <br />
            <asp:TextBox ID="txtNotificationTitle" runat="server" /><br />
            <br />
            Notification Sub Title<br />
            <br />
            <asp:TextBox ID="txtSubTitle" runat="server" /><br />
            <br />
            Notification Body<br />
            <br />
            <asp:TextBox ID="txtNotificationBody" runat="server" /><br />
            <br />
            <asp:Button ID="btnSubmit" runat="server" OnClientClick="javascript:return validate();"
                CssClass="submit" Text="Submit" OnClick="btnSubmit_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
